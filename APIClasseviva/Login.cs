﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Classeviva;
//
//    var data = Login.FromJson(jsonString);
//
namespace Classeviva
{
	using System.Text.RegularExpressions;

	using Newtonsoft.Json;

	public class Login : IResponse
	{
		
		[JsonProperty("expire")]
		public string Expire { get; set; }
		[JsonProperty("firstName")]
		public string FirstName { get; set; }
		[JsonProperty("ident")]
		public string Ident { get; set; }
		[JsonProperty("lastName")]
		public string LastName { get; set; }
		[JsonProperty("release")]
		public string Release { get; set; }
		[JsonProperty("token")]
		public string Token { get; set; }

		public static Login FromJson(string json) => JsonConvert.DeserializeObject<Login>(json, Converter.Settings);

		IResponse IResponse.FromJson(string s) => FromJson(s);

		public int GetSID() => int.Parse(Regex.Replace(Ident, "[^0-9.]", ""));
		public override string ToString() => $"Nome: {FirstName}\tCognome: {LastName}\tCodice studente: {Ident}";
	}

	public static partial class Serialize
	{
		public static string ToJson(this Login self)
		{
			return JsonConvert.SerializeObject(self, Converter.Settings);
		}
	}
}
