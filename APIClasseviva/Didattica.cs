﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Classeviva;
//
//    var data = Didattica.FromJson(jsonString);
//
namespace Classeviva
{
	using System;
	using System.Collections.Generic;
	using System.Globalization;

	using Newtonsoft.Json;

	public partial class Didattica : IResponse
    {
        [JsonProperty("didacticts")]
        public List<Didactict> Didacticts { get; set; }

        public static Didattica FromJson(string json) => JsonConvert.DeserializeObject<Didattica>(json, Converter.Settings);
        IResponse IResponse.FromJson(string json) => FromJson(json);

        public override string ToString()
        {
            string s = "";
            foreach (var item in Didacticts)
                s += item.ToString() + "\n\n";
            return s.Substring(0, s.Length - 2);
        }
    }

    public partial class Didactict
    {
        [JsonProperty("folders")]
        public List<Folder> Folders { get; set; }

        [JsonProperty("teacherFirstName")]
        public string TeacherFirstName { get; set; }

        [JsonProperty("teacherId")]
        public string TeacherId { get; set; }

        [JsonProperty("teacherLastName")]
        public string TeacherLastName { get; set; }

        [JsonProperty("teacherName")]
        public string TeacherName { get; set; }

        public override string ToString()
        {
            string s = $"{TeacherName}\tCartelle:\n";
            foreach (var item in Folders)
                s += $"\t{item.ToString()}\n\n";
            return s.Substring(0, s.Length - 2);
        }
    }

    public partial class Folder
    {
        [JsonProperty("contents")]
        public List<Content> Contents { get; set; }

        [JsonProperty("folderId")]
        public double FolderId { get; set; }

        [JsonProperty("folderName")]
        public string FolderName { get; set; }

        [JsonProperty("lastShareDT")]
        public string LastShare { get; set; }

        public DateTime LastShareDT { get => DateTime.Parse(LastShare); }

        public override string ToString()
        {
            string fn = FolderName == "Uncategorized" ? "Altro" : FolderName;
            string s = $"{fn}\tUltimo upload: {LastShareDT.ToString("G", DateTimeFormatInfo.InvariantInfo)}\tContenuto:\n";
            foreach (var item in Contents)
                s += $"\t{item.ToString()}\n";
            return s.Substring(0, s.Length - 1);
        }
    }

    public partial class Content
    {
        [JsonProperty("contentId")]
        public long ContentId { get; set; }

        [JsonProperty("contentName")]
        public string ContentName { get; set; }

        [JsonProperty("objectId")]
        public long ObjectId { get; set; }

        [JsonProperty("objectType")]
        public string ObjectType { get; set; }

        [JsonProperty("shareDT")]
        public string ShareDT { get; set; }

        public override string ToString() => $"{ObjectType}\t{ContentName}\t{ShareDT}";
    }
    
    public static partial class Serialize
    {
        public static string ToJson(this Didattica self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }
}
