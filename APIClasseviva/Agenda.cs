﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Classeviva;
//
//    var data = Agenda.FromJson(jsonString);
//
namespace Classeviva
{
	using System.Collections.Generic;

	using Newtonsoft.Json;

	public partial class Agenda : IResponse
    {
        [JsonProperty("agenda")]
        public List<PurpleAgenda> PurpleAgenda { get; set; }

        public static Agenda FromJson(string json) => JsonConvert.DeserializeObject<Agenda>(json, Converter.Settings);
        IResponse IResponse.FromJson(string s) => FromJson(s);
    }

    public partial class PurpleAgenda
    {
        [JsonProperty("authorName")]
        public string AuthorName { get; set; }

        [JsonProperty("classDesc")]
        public string ClassDesc { get; set; }

        [JsonProperty("evtCode")]
        public string EvtCode { get; set; }

        [JsonProperty("evtDatetimeBegin")]
        public string EvtDatetimeBegin { get; set; }

        [JsonProperty("evtDatetimeEnd")]
        public string EvtDatetimeEnd { get; set; }

        [JsonProperty("evtId")]
        public long EvtId { get; set; }

        [JsonProperty("isFullDay")]
        public bool IsFullDay { get; set; }

        [JsonProperty("notes")]
        public string Notes { get; set; }

        [JsonProperty("subjectDesc")]
        public string SubjectDesc { get; set; }
    }
    
    public static partial class Serialize
    {
        public static string ToJson(this Agenda self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }
}
