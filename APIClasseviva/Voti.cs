﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Classeviva;
//
//    var data = Voti.FromJson(jsonString);
//
namespace Classeviva
{
	using System.Collections.Generic;

	using Newtonsoft.Json;

	public partial class Grade
    {
        [JsonProperty("canceled")]
        public bool Canceled { get; set; }

        [JsonProperty("color")]
        public Color Color { get; set; }

        [JsonProperty("componentDesc")]
        public string ComponentDesc { get; set; }

        [JsonProperty("componentPos")]
        public long ComponentPos { get; set; }

        [JsonProperty("decimalValue")]
        public double? DecimalValue { get; set; }

        [JsonProperty("displaPos")]
        public long DisplaPos { get; set; }

        [JsonProperty("displayValue")]
        public string DisplayValue { get; set; }

        [JsonProperty("evtCode")]
        public string EvtCode { get; set; }

        [JsonProperty("evtDate")]
        public string EvtDate { get; set; }

        [JsonProperty("evtId")]
        public long EvtId { get; set; }

        [JsonProperty("notesForFamily")]
        public string NotesForFamily { get; set; }

        [JsonProperty("periodDesc")]
        public string PeriodDesc { get; set; }

        [JsonProperty("periodPos")]
        public long PeriodPos { get; set; }

        [JsonProperty("subjectCode")]
        public string SubjectCode { get; set; }

        [JsonProperty("subjectDesc")]
        public string SubjectDesc { get; set; }

        [JsonProperty("subjectId")]
        public long SubjectId { get; set; }

        [JsonProperty("underlined")]
        public bool Underlined { get; set; }

        [JsonProperty("weightFactor")]
        public long WeightFactor { get; set; }

        public override string ToString()
        {
            return $"Voto: {DisplayValue}\tMateria: {SubjectDesc}\nNote per la famiglia: {NotesForFamily}\nData: {EvtDate}\tTipo: {ComponentDesc}";
        }
    }

    public enum Color { Blue, Green, Red };
    
    public partial class Voti : IResponse
    {
        [JsonProperty("grades")]
        public List<Grade> Grades { get; set; }

        public static Voti FromJson(string json, string s) => JsonConvert.DeserializeObject<Voti>(json, Converter.Settings);

        IResponse IResponse.FromJson(string json) => FromJson(json, "");
        
        public override string ToString()
        {
            string s = "";
            foreach (var item in Grades)
                s += item.ToString() + "\n\n";
            return s.Substring(0, s.Length - 2);
        }

        public static Color GetColor(double voto) => voto < 5.75 ? Color.Red : Color.Green;
    }
    
    public static partial class Serialize
    {
        public static string ToJson(this Voti self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

}
