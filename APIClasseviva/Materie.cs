﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Classeviva;
//
//    var data = Materie.FromJson(jsonString);
//
namespace Classeviva
{
	using System.Collections.Generic;

	using Newtonsoft.Json;

	public class Materie : IResponse
    {
        [JsonProperty("subjects")]
        public List<Subject> Subjects { get; set; }

        public static Materie FromJson(string json) => JsonConvert.DeserializeObject<Materie>(json, new JsonSerializerSettings {MetadataPropertyHandling = MetadataPropertyHandling.Ignore,DateParseHandling = DateParseHandling.None});
        IResponse IResponse.FromJson(string s) => FromJson(s);

        public override string ToString()
        {
            string s = "";
            foreach (var item in Subjects)
                s += item.ToString() + "\n\n";
            return s.Substring(0, s.Length - 2);
        }
    }

    public class Subject
    {
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("order")]
        public long Order { get; set; }

        [JsonProperty("teachers")]
        public List<Teacher> Teachers { get; set; }

        public override string ToString()
        {
            string s = "";
            foreach (var item in Teachers)
                s += item.ToString() + "\t";
            return $"{Description}\tProf: {s}";
        }
    }

    public class Teacher
    {
        [JsonProperty("teacherId")]
        public string TeacherId { get; set; }

        [JsonProperty("teacherName")]
        public string TeacherName { get; set; }

        public override string ToString() => TeacherName;
    }
    
    public static partial class Serialize
    {
        public static string ToJson(this Materie self) => JsonConvert.SerializeObject(self, new JsonSerializerSettings {MetadataPropertyHandling = MetadataPropertyHandling.Ignore,DateParseHandling = DateParseHandling.None});
    }
}

