﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Classeviva;
//
//    var data = BachecaRead.FromJson(jsonString);
//
namespace Classeviva
{
	using Newtonsoft.Json;

	public partial class BachecaRead : IResponse
    {
        [JsonProperty("item")]
        public Item Element { get; set; }

        public static BachecaRead FromJson(string json) => JsonConvert.DeserializeObject<BachecaRead>(json, Converter.Settings);
        IResponse IResponse.FromJson(string s) => FromJson(s);

        public override string ToString() => Element.ToString();

        public partial class Item
        {
            [JsonProperty("text")]
            public string Text { get; set; }

            [JsonProperty("title")]
            public string Title { get; set; }

            public override string ToString() => $"{Text}\n{Title}";
        }
    }
    
    public static partial class Serialize
    {
        public static string ToJson(this BachecaRead self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }
}
