﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Classeviva;
//
//    var data = Errore.FromJson(jsonString);
//
namespace Classeviva
{
	using Newtonsoft.Json;

	public class Errore : IResponse
    {
        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("statusCode")]
        public long StatusCode { get; set; }

        public static Errore FromJson(string json) => JsonConvert.DeserializeObject<Errore>(json, new JsonSerializerSettings {MetadataPropertyHandling = MetadataPropertyHandling.Ignore,DateParseHandling = DateParseHandling.None});
        IResponse IResponse.FromJson(string s) => FromJson(s);
    }

    public static partial class Serialize
    {
        public static string ToJson(this Errore self) => JsonConvert.SerializeObject(self, new JsonSerializerSettings {MetadataPropertyHandling = MetadataPropertyHandling.Ignore,DateParseHandling = DateParseHandling.None});
    }
}
