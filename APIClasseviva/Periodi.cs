﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Classeviva;
//
//    var data = Periodi.FromJson(jsonString);
//
namespace Classeviva
{
	using System.Collections.Generic;

	using Newtonsoft.Json;

	public class Periodi : IResponse
    {
        [JsonProperty("periods")]
        public List<Period> Periods { get; set; }

        public static Periodi FromJson(string json) => JsonConvert.DeserializeObject<Periodi>(json, Converter.Settings);
        IResponse IResponse.FromJson(string s) => FromJson(s);

        public override string ToString()
        {
            string s = "";
            foreach (var item in Periods)
                s += item.ToString() + "\n\n";
            return s.Substring(0, s.Length - 2);
        }
    }

    public partial class Period
    {
        [JsonProperty("dateEnd")]
        public string DateEnd { get; set; }

        [JsonProperty("dateStart")]
        public string DateStart { get; set; }

        [JsonProperty("isFinal")]
        public bool IsFinal { get; set; }

        [JsonProperty("miurDivisionCode")]
        public object MiurDivisionCode { get; set; }

        [JsonProperty("periodCode")]
        public string PeriodCode { get; set; }

        [JsonProperty("periodDesc")]
        public string PeriodDesc { get; set; }

        [JsonProperty("periodPos")]
        public long PeriodPos { get; set; }

        public override string ToString() => $"Nome: {PeriodDesc}\tInizio: {DateStart}\tFine: {DateEnd}\tPeriodo finale: {IsFinal}";
    }
    
    public static partial class Serialize
    {
        public static string ToJson(this Periodi self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }
}
