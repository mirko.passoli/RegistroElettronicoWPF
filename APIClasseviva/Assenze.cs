﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Classeviva;
//
//    var data = Assenze.FromJson(jsonString);
//
namespace Classeviva
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel;
	using System.Globalization;

	using Newtonsoft.Json;

	public partial class Assenze : IResponse
    {
        [JsonProperty("events")]
        public List<Event> Events { get; set; }

        public static Assenze FromJson(string json) => JsonConvert.DeserializeObject<Assenze>(json, Converter.Settings);
        IResponse IResponse.FromJson(string s) => FromJson(s);

        public override string ToString()
        {
            string s = "";
            foreach (var item in Events)
                s += item.ToString() + "\n\n";
            return s.Substring(0, s.Length - 2);
        }
    }

    public partial class Event
    {
        public enum EventType
        {
            [Description("ABA")]
            A,
            [Description("ABR")]
            R,
            [Description("ABU")]
            U
        }

        [JsonProperty("evtCode")]
        public string EvtCode { get; set; }

        [JsonProperty("evtDate")]
        public string EvtDate { get; set; }
        
        [JsonProperty("evtHPos")]
        public long? EvtHPos { get; set; }

        [JsonProperty("evtId")]
        public long EvtId { get; set; }

        [JsonProperty("evtValue")]
        public long? EvtValue { get; set; }

        [JsonProperty("isJustified")]
        public bool IsJustified { get; set; }

        [JsonProperty("justifReasonCode")]
        public string JustifReasonCode { get; set; }

        [JsonProperty("justifReasonDesc")]
        public string JustifReasonDesc { get; set; }

        public DateTime EvtDT { get => DateTime.ParseExact(EvtDate, "yyyy-MM-dd", CultureInfo.InvariantCulture); }

        public EventType EvtType { get => (EventType)Enum.Parse(typeof(EventType), EvtCode.Substring(0, 3)); }

        public override string ToString() => $"Tipo: {EvtType}";
    }
    
    public static partial class Serialize
    {
        public static string ToJson(this Assenze self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }
}
