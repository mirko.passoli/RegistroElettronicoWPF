﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Classeviva;
//
//    var data = DidatticaItem.FromJson(jsonString);
//
namespace Classeviva
{
	using Newtonsoft.Json;

	public partial class DidatticaItem : IResponse
    {
        [JsonProperty("item")]
        public Item Item { get; set; }

        public static DidatticaItem FromJson(string json) => JsonConvert.DeserializeObject<DidatticaItem>(json, Converter.Settings);
        IResponse IResponse.FromJson(string json) => FromJson(json);
    }

    public partial class Item
    {
        [JsonProperty("link")]
        public string Link { get; set; }
    }
    
    public static partial class Serialize
    {
        public static string ToJson(this DidatticaItem self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }
}