﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Classeviva;
//
//    var data = Bacheca.FromJson(jsonString);
//
namespace Classeviva
{
	using System;
	using System.Collections.Generic;

	using Newtonsoft.Json;

	public partial class Bacheca : IResponse
    {
        [JsonProperty("items")]
        public List<Item> Items { get; set; }

        public static Bacheca FromJson(string json) => JsonConvert.DeserializeObject<Bacheca>(json, Converter.Settings);
        IResponse IResponse.FromJson(string json) => FromJson(json);
    }

    public partial class Item
    {
        [JsonProperty("cntCategory")]
        public string CntCategory { get; set; }

        [JsonProperty("cntHasAttach")]
        public bool CntHasAttach { get; set; }

        [JsonProperty("cntHasChanged")]
        public bool CntHasChanged { get; set; }

        [JsonProperty("cntId")]
        public long CntId { get; set; }

        [JsonProperty("cntStatus")]
        public string CntStatus { get; set; }

        [JsonProperty("cntTitle")]
        public string CntTitle { get; set; }

        [JsonProperty("cntValidFrom")]
        public string CntValidFrom { get; set; }

        [JsonProperty("cntValidInRange")]
        public bool CntValidInRange { get; set; }

        [JsonProperty("cntValidTo")]
        public string CntValidTo { get; set; }

        [JsonProperty("evtCode")]
        public string EvtCode { get; set; }

        [JsonProperty("needFile")]
        public bool NeedFile { get; set; }

        [JsonProperty("needJoin")]
        public bool NeedJoin { get; set; }

        [JsonProperty("needReply")]
        public bool NeedReply { get; set; }

        [JsonProperty("pubDT")]
        public string Pub { get; set; }

        public DateTime PubDT { get => DateTime.Parse(Pub); }

        [JsonProperty("pubId")]
        public long PubId { get; set; }

        [JsonProperty("readStatus")]
        public bool ReadStatus { get; set; }

        public override string ToString() => $"{CntTitle}\t{PubDT.Day}/{PubDT.Month}\n{CntCategory}\tAllegato: {CntHasAttach}";
    }
    
    public static partial class Serialize
    {
        public static string ToJson(this Bacheca self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }
}
