﻿namespace Classeviva
{
	public interface IResponse
    {
        IResponse FromJson(string s);
    }
}
