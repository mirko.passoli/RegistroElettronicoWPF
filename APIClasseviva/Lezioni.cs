﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Classeviva;
//
//    var data = Lezioni.FromJson(jsonString);
//
namespace Classeviva
{
	using System.Collections.Generic;

	using Newtonsoft.Json;

	public partial class Lezioni : IResponse
    {
        [JsonProperty("lessons")]
        public List<Lesson> Lessons { get; set; }

        public static Lezioni FromJson(string json) => JsonConvert.DeserializeObject<Lezioni>(json, new JsonSerializerSettings {MetadataPropertyHandling = MetadataPropertyHandling.Ignore,DateParseHandling = DateParseHandling.None});
        IResponse IResponse.FromJson(string s) => FromJson(s);

        public override string ToString()
        {
            string s = "";
            foreach (var item in Lessons)
                s += item.ToString() + "\n\n";
            return s.Substring(0, s.Length - 2);
        }
    }

    public partial class Lesson
    {
        [JsonProperty("authorName")]
        public string AuthorName { get; set; }

        [JsonProperty("classDesc")]
        public string ClassDesc { get; set; }

        [JsonProperty("evtCode")]
        public string EvtCode { get; set; }

        [JsonProperty("evtDate")]
        public string EvtDate { get; set; }

        [JsonProperty("evtDuration")]
        public long EvtDuration { get; set; }

        [JsonProperty("evtHPos")]
        public long EvtHPos { get; set; }

        [JsonProperty("evtId")]
        public long EvtId { get; set; }

        [JsonProperty("lessonArg")]
        public string LessonArg { get; set; }

        [JsonProperty("lessonType")]
        public string LessonType { get; set; }

        [JsonProperty("subjectCode")]
        public string SubjectCode { get; set; }

        [JsonProperty("subjectDesc")]
        public string SubjectDesc { get; set; }

        [JsonProperty("subjectId")]
        public long SubjectId { get; set; }

        public override string ToString()
        {
            return $"{EvtDate} - {EvtHPos}° ora\t{SubjectDesc}\t{AuthorName}\n{LessonArg}";
        }
    }
    
    public static partial class Serialize
    {
        public static string ToJson(this Lezioni self) => JsonConvert.SerializeObject(self, new JsonSerializerSettings {MetadataPropertyHandling = MetadataPropertyHandling.Ignore,DateParseHandling = DateParseHandling.None});
    }
}

