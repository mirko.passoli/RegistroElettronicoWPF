﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Classeviva;
//
//    var data = UInfo.FromJson(jsonString);
//
namespace Classeviva
{
	using System.Collections.Generic;

	using Newtonsoft.Json;

	public partial class UInfo : IResponse
    {
        [JsonProperty("cards")]
        public List<Card> Cards { get; set; }

        public static UInfo FromJson(string json) => JsonConvert.DeserializeObject<UInfo>(json, Converter.Settings);
        IResponse IResponse.FromJson(string s) => FromJson(s);

        public override string ToString()
        {
            string s = "";
            foreach (var item in Cards)
                s += item.ToString() + "\n\n";
            return s.Substring(0, s.Length - 2);
        }
    }

    public partial class Card
    {
        [JsonProperty("birthDate")]
        public string BirthDate { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("fiscalCode")]
        public string FiscalCode { get; set; }

        [JsonProperty("ident")]
        public string Ident { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("miurDivisionCode")]
        public string MiurDivisionCode { get; set; }

        [JsonProperty("miurSchoolCode")]
        public string MiurSchoolCode { get; set; }

        [JsonProperty("schCity")]
        public string SchCity { get; set; }

        [JsonProperty("schCode")]
        public string SchCode { get; set; }

        [JsonProperty("schDedication")]
        public string SchDedication { get; set; }

        [JsonProperty("schName")]
        public string SchName { get; set; }

        [JsonProperty("schProv")]
        public string SchProv { get; set; }

        [JsonProperty("usrId")]
        public long UsrId { get; set; }

        [JsonProperty("usrType")]
        public string UsrType { get; set; }

        public override string ToString() => $"{FirstName} {LastName}\n{SchName} {SchDedication}\t{SchCode}";
    }

    public static partial class Serialize
    {
        public static string ToJson(this UInfo self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }
}
