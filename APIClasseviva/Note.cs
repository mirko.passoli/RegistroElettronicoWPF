﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Classeviva;
//
//    var data = Note.FromJson(jsonString);
//
namespace Classeviva
{
	using System.Collections.Generic;

	using Newtonsoft.Json;

	public partial class Note : IResponse
    {
        [JsonProperty("NTCL")]
        public List<NTTE> NTCL { get; set; }

        [JsonProperty("NTST")]
        public List<NTTE> NTST { get; set; }

        [JsonProperty("NTTE")]
        public List<NTTE> NTTE { get; set; }

        [JsonProperty("NTWN")]
        public List<NTTE> NTWN { get; set; }

        public List<NTTE> GetAllNotes()
        {
            List<NTTE> a = new List<NTTE>();
            a.AddRange(NTCL);
            a.AddRange(NTST);
            a.AddRange(NTTE);
            a.AddRange(NTWN);
            return a;
        }

        public static Note FromJson(string json) => JsonConvert.DeserializeObject<Note>(json, new JsonSerializerSettings {MetadataPropertyHandling = MetadataPropertyHandling.Ignore,DateParseHandling = DateParseHandling.None});
        IResponse IResponse.FromJson(string s) => FromJson(s);

        public override string ToString()
        {
            string s = "";
            foreach (var item in GetAllNotes())
                s += item.ToString() + "\n\n";
            return s.Substring(0, s.Length - 2);
        }
    }

    public partial class NTTE
    {
        [JsonProperty("authorName")]
        public string AuthorName { get; set; }

        [JsonProperty("evtDate")]
        public string EvtDate { get; set; }

        [JsonProperty("evtId")]
        public long EvtId { get; set; }

        [JsonProperty("evtText")]
        public string EvtText { get; set; }

        [JsonProperty("readStatus")]
        public bool ReadStatus { get; set; }

        [JsonProperty("warningType")]
        public string WarningType { get; set; }

        public override string ToString() => $"{EvtDate}\t{AuthorName}\t{EvtText}";
    }
    
    public static partial class Serialize
    {
        public static string ToJson(this Note self) => JsonConvert.SerializeObject(self, new JsonSerializerSettings {MetadataPropertyHandling = MetadataPropertyHandling.Ignore,DateParseHandling = DateParseHandling.None});
    }
}
