﻿using System.IO;
using System.Net;

using RestSharp;

namespace Classeviva
{
	public class API
	{
		private const string BASE_URL = "https://web.spaggiari.eu/rest/v1/";

		private static RestClient client = new RestClient(BASE_URL)
		{
			UserAgent = "zorro/1.0"
		};
		
		private static RestRequest MakeRequest(string url, Method method, string postData, string token)
		{
			RestRequest request = new RestRequest(url, method);

			request.AddHeader("ContentType", "application/json");
			request.AddHeader("Z-Dev-Apikey", "+zorro+");

			if (token != null)
				request.AddHeader("Z-Auth-Token", token);
			if (method == Method.POST && postData != null)
				request.AddParameter("application/json", postData, ParameterType.RequestBody);

			return request;
		}
		
		private static IRestResponse GetResponse(string url, Method method, string postData, string token)
		{
			var request = MakeRequest(url, method, postData, token);
			return client.Execute(request);
		}
		
		private static IResponse Response(IResponse type, string url, Method method, string postData, string token)
		{
			var response = GetResponse(url, method, postData, token);
			string s = response.Content;
			if (response.StatusCode != HttpStatusCode.OK)
				return Errore.FromJson(s);
			else
				return type.FromJson(s);
		}

		private static IResponse GetIResponse(IResponse type, string url, string token) => Response(type, url, Method.GET, null, token);

		private static IResponse PostIResponse(IResponse type, string url, string postData, string token) => Response(type, url, Method.POST, postData, token);

		public static IResponse Login(string uid, string pass)
		{
			string data = "{ \"pass\" : \"" + pass + "\", \"uid\" : \"" + uid + "\" }";
			return PostIResponse(new Login(), "auth/login", data, null);
		}

		public static IResponse Voti(string token, int sid) => GetIResponse(new Voti(), $"students/{sid}/grades", token);

		public static IResponse Lezioni(string token, int sid) => GetIResponse(new Lezioni(), $"students/{sid}/lessons/today", token);

		public static IResponse Lezioni(string token, int sid, int date) => GetIResponse(new Lezioni(), $"students/{sid}/lessons/{date}", token);

		public static IResponse Lezioni(string token, int sid, int dateStart, int dateEnd) => GetIResponse(new Lezioni(), $"students/{sid}/lessons/{dateStart}/{dateEnd}", token);

		public static IResponse Note(string token, int sid) => GetIResponse(new Note(), $"students/{sid}/notes/all", token);

		public static IResponse Materie(string token, int sid) => GetIResponse(new Materie(), $"students/{sid}/subjects", token);

		public static IResponse Periodi(string token, int sid) => GetIResponse(new Periodi(), $"students/{sid}/periods", token);

		public static IResponse UInfo(string token, int sid) => GetIResponse(new UInfo(), $"students/{sid}/cards", token);

		public static IResponse Didattica(string token, int sid) => GetIResponse(new Didattica(), $"students/{sid}/didactics", token);

		public static IResponse DidatticaGetLink(string token, int sid, long contentId) => GetIResponse(new DidatticaItem(), $"students/{sid}/didactics/item/{contentId}", token);

		public static string DidatticaDownloadFile(string token, int sid, long contentId, string path)
		{
			var request = MakeRequest($"students/{sid}/didactics/item/{contentId}", Method.GET, null, token);
			string tfile = Path.GetTempFileName();
			string name = "";
			using (var writer = File.OpenWrite(tfile))
			{
				request.ResponseWriter = (responseStream) => responseStream.CopyTo(writer);
				var response = client.Execute(request);

				foreach (var item in response.Headers)
				{
					if (item.Name == "Content-Disposition")
					{
						name = item.Value.ToString().Split('"')[1];
						break;
					}
				}

			}
			File.Copy(tfile, path + name, true);
			return path + name;
		}

		public static IResponse Bacheca(string token, int sid) => GetIResponse(new Bacheca(), $"students/{sid}/noticeboard", token);

		public static IResponse BachecaRead(string token, int sid, string eventCode, int pubId) => PostIResponse(new BachecaRead(), $"students/{sid}/noticeboard/read/{eventCode}/{pubId}/101", null, token);

		public static string BachecaAttachDownload(string token, int sid, string eventCode, int pubId, string path)
		{
			var request = MakeRequest($"students/{sid}/noticeboard/attach/{eventCode}/{pubId}/101", Method.GET, null, token);
			string tfile = Path.GetTempFileName();
			string name = "";
			using (var writer = File.OpenWrite(tfile))
			{
				request.ResponseWriter = (responseStream) => responseStream.CopyTo(writer);
				var response = client.Execute(request);

				foreach (var item in response.Headers)
				{
					if (item.Name == "Content-Disposition")
					{
						name = item.Value.ToString().Split('"')[1];
						break;
					}
				}

			}
			File.Copy(tfile, path + name, true);
			return path + name;
		}

		public static IResponse Assenze(string token, int sid) => GetIResponse(new Assenze(), $"students/{sid}/absences/details", token);

		public static IResponse Agenda(string token, int sid, int from, int to) => GetIResponse(new Agenda(), $"students/{sid}/agenda/all/{from}/{to}", token);
	}
}
