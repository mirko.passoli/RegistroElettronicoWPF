﻿using Classeviva;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace RegistroElettronicoWPF
{
    /// <summary>
    /// Logica di interazione per ControlVoto.xaml
    /// </summary>
    public partial class ControlVoto : UserControl
    {
        public string Data { get => lblData.Content.ToString(); private set => lblData.Content = value; }
        public string Tipo { get => Grade.ComponentDesc; }
        public string Desc { get => Grade.NotesForFamily; }
        public string Voto { get => Grade.DisplayValue; }
        public double? VotoD { get => Grade.DecimalValue; }
        public int Periodo { get => int.Parse(Data.Substring(3)) < 9 ? 2 : 1; }

        public Grade Grade { get; private set; }

        public ControlVoto(Grade voto = null)
        {
            InitializeComponent();

            if (voto == null)
                return;

            Grade = voto;
            Data = DateTime.Parse(voto.EvtDate).ToString("dd/MM");
            
            lblTipo.Text = Tipo + (string.IsNullOrWhiteSpace(Desc) ? "" : "\n" + Desc);
            prgVoto.Voto = voto;
        }
        
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            MessageBox.Show($"Voto: {Voto}\tTipo di prova: {Tipo}\t{Data}\nNote per la famiglia: {Desc}", "Dettagli voto");
        }
    }
}
