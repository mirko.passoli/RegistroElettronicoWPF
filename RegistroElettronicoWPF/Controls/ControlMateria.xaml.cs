﻿using Classeviva;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace RegistroElettronicoWPF
{
    /// <summary>
    /// Logica di interazione per ControlMateria.xaml
    /// </summary>
    public partial class ControlMateria : UserControl
    {
        public bool Separator { get => Separatore.Visibility == Visibility.Visible; set => Separatore.Visibility = value ? Visibility.Visible : Visibility.Collapsed; }
        public List<Grade> Voti { get => _voti; }

        private List<Grade> _voti;

        public ControlMateria(List<Grade> votiMateria = null)
        {
            if (votiMateria == null || votiMateria.Count == 0)
                return;

            _voti = new List<Grade>(votiMateria);
            
            InitializeComponent();
            lblMateria.Text = votiMateria[0].SubjectDesc;

            prgVoto.Voto = new Grade() { DisplayValue = $"{Media():f2}", DecimalValue = Media(), Color = Classeviva.Voti.GetColor(Media()) };
        }
        
        public ControlMateria(string v1, double v2)
        {
            InitializeComponent();
            lblMateria.Text = v1;
            prgVoto.Voto = new Grade() { DecimalValue = v2, DisplayValue = $"{v2:f2}", Color = Classeviva.Voti.GetColor(v2) };
        }

        public ControlMateria(ControlMateria materia)
        {
            InitializeComponent();

            Separator = materia.Separator;
            _voti = materia.Voti;

            lblMateria.Text = _voti[0].SubjectDesc;
            prgVoto.Voto = new Grade() { DecimalValue = Media(), DisplayValue = $"{Media():f2}", Color = Classeviva.Voti.GetColor(Media()) };
        }

        private void AddControl(Control c)
        {
            Container.Children.Add(c);
            Height = Height + c.Height;
        }

        private double Media()
        {
            double val = 0;
            int count = 0;

            foreach (var item in _voti)
            {
                if (item != null && item.DecimalValue.HasValue)
                {
                    count++;
                    val += item.DecimalValue.Value;
                }

            }

            return val / (double)count;
        }

        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);
            WindowDettagliMateria wdm = new WindowDettagliMateria(this, $"Dettaglio {_voti[0].SubjectDesc.ToLower()} - {_voti[0].PeriodDesc}");
            wdm.Show();
        }
    }
}
