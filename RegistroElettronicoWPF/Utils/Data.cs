﻿using Classeviva;
using Newtonsoft.Json;
using System;
using System.ComponentModel;

namespace RegistroElettronicoWPF
{
    internal class Data
    {
        internal static Data instance = null;

        public Login Login { get; set; }
        public string Mail { get; set; }
        public string Password { get; set; }

        internal string SessionID { get; private set; }

        private BackgroundWorker bwSave;

        public Data()
        {
            instance = this;
            bwSave = new BackgroundWorker();
            bwSave.DoWork += SAsync;
        }

        private void SAsync(object sender, DoWorkEventArgs e)
        {
            Save();
        }

        public void Save()
        {
            Properties.Settings.Default.Data = JsonConvert.SerializeObject(this);
            Properties.Settings.Default.Save();
        }

        internal bool ValidLogin() => Login != null && !string.IsNullOrEmpty(Login.Token) && DateTime.Parse(Login.Expire) > DateTime.Now;
        
        internal void SaveAsync() => bwSave.RunWorkerAsync();
    }
}
