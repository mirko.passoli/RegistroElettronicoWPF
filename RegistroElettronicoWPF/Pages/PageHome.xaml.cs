﻿using Classeviva;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace RegistroElettronicoWPF
{
    /// <summary>
    /// Logica di interazione per PageHome.xaml
    /// </summary>
    public partial class PageHome : Page
    {
        private BackgroundWorker bw_agenda;
        private BackgroundWorker bw_file;
        private BackgroundWorker bw_note;
        private BackgroundWorker bw_profilo;
        private BackgroundWorker bw_voti;

        public PageHome()
        {
            InitializeComponent();

            #region Init BackgroundWorkers
            #region Agenda
            bw_agenda = new BackgroundWorker();
            bw_agenda.WorkerSupportsCancellation = true;

            #endregion

            #region File
            bw_file = new BackgroundWorker();
            bw_file.WorkerReportsProgress = true;
            bw_file.WorkerSupportsCancellation = true;
            bw_file.DoWork += BWFileDoWork;
            bw_file.ProgressChanged += BWFileProgressChanged;

            #endregion

            #region Note
            bw_note = new BackgroundWorker();
            bw_note.WorkerSupportsCancellation = true;

            #endregion

            #region Profilo
            bw_profilo = new BackgroundWorker();
            bw_profilo.WorkerReportsProgress = true;
            bw_profilo.WorkerSupportsCancellation = true;
            bw_profilo.DoWork += BWProfiloDoWork;
            bw_profilo.ProgressChanged += BWProfiloProgressChanged;
            #endregion

            #region Voti
            bw_voti = new BackgroundWorker();
            bw_voti.WorkerSupportsCancellation = true;
            bw_voti.DoWork += BWVotiDoWork;
            #endregion

            #region Run
            if (!WindowMain.OFFLINE)
            {
                bw_file.RunWorkerAsync();
                bw_profilo.RunWorkerAsync();
                bw_voti.RunWorkerAsync();
            }
            #endregion
            #endregion
        }


        #region BackgroundWorkers events

        #region DoWork

        private void BWProfiloDoWork(object sender, DoWorkEventArgs e)
        {
            DoWork(e, () => {
                Data d = Data.instance;
                int sid = int.Parse(Regex.Replace(d.Login.Ident, "[^0-9.]", ""));

                IResponse info = API.UInfo(d.Login.Token, sid);
                bw_profilo.ReportProgress(info is UInfo ? 1 : 0, info);
            });
        }

        private void BWVotiDoWork(object sender, DoWorkEventArgs e)
        {
            DoWork(e, () => {
                Data d = Data.instance;
                int sid = int.Parse(Regex.Replace(d.Login.Ident, "[^0-9.]", ""));

                IResponse voti = API.Voti(d.Login.Token, sid);
                if (voti is Voti v)
                    Dispatcher.Invoke(() => ParseVoti(v));
            });
        }

        private void BWFileDoWork(object sender, DoWorkEventArgs e)
        {
            DoWork(e, () =>
            {
                Data d = Data.instance;
                int sid = int.Parse(Regex.Replace(d.Login.Ident, "[^0-9.]", ""));

                IResponse response = API.Didattica(d.Login.Token, sid);
                bw_file.ReportProgress(response is Didattica ? 1 : 0, response);
            });
        }

        private void FileDownloaderDoWork(object sender, DoWorkEventArgs e)
        {
            if (e.Argument is object[] a)
            {
                if (a[0] is Content b && a[1] is MessageBoxResult mbrSalva && a[2] is string folder && a[3] is MessageBoxResult mbrApriAlTermine)
                {
                    Data data = Data.instance;
                    int sid = int.Parse(Regex.Replace(data.Login.Ident, "[^0-9.]", ""));
                    WebResponse dfileinfo = API.DidatticaGetFile(data.Login.Token, sid, b.ContentId);

                    string filepath = folder + dfileinfo.Headers.Get("Content-Disposition").Split('"')[1];

                    if (dfileinfo != null)
                    {
                        FileStream fs = null;
                        WindowProgress wp = null;

                        try
                        {
                            fs = new FileStream(filepath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None);
                            byte[] buffer = new byte[1024];

                            Dispatcher.Invoke(() =>
                            {
                                wp = new WindowProgress();
                                wp.LabelText = "Downloading " + filepath.Split('\\').Last();
                                wp.Show();
                            });

                            Stream input = (dfileinfo as WebResponse).GetResponseStream();
                            double recived = 0;
                            double total = (int)(dfileinfo as WebResponse).ContentLength;
                            int size = input.Read(buffer, 0, buffer.Length);

                            while (size > 0)
                            {
                                fs.Write(buffer, 0, size);
                                recived += size;
                                size = input.Read(buffer, 0, buffer.Length);
                                Dispatcher.Invoke(() => { wp.ProgressValue = (recived * 100) / total; });
                            }
                            input.Close();

                            if (mbrSalva == MessageBoxResult.No || mbrApriAlTermine == MessageBoxResult.Yes)
                            {
                                try
                                {
                                    Process.Start(filepath);
                                }
                                catch (Win32Exception ex)
                                {
                                    MessageBox.Show(ex.Message);
                                }
                            }
                        }
                        catch (Exception ex) { MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error); }
                        finally
                        {
                            if (fs != null)
                            {
                                fs.Flush();
                                fs.Close();
                                fs.Dispose();
                            }
                            if (wp != null)
                                Dispatcher.Invoke(() => { wp.Close(); });
                        }
                    }
                }
            }
        }

        #region Other methods

        private void DoWork(DoWorkEventArgs e, Action valid)
        {
            int a = 0;
            if (e.Argument != null)
            {
                a = (int)e.Argument;
                if ((int)e.Argument >= 5)
                    return;
            }

            Data d = Data.instance;

            try
            {
                if (d.ValidLogin())
                {
                    valid();
                }
                else
                {
                    IResponse apiresponse = API.Login(d.Mail, d.Password);
                    if (apiresponse is Login)
                    {
                        d.Login = (Login)apiresponse;
                        d.Save();
                    }
                    DoWork(new DoWorkEventArgs(a + 1), valid);
                }
            }
            catch (Exception) { }
        }

        private void ParseVoti(Voti v)
        {
            long[] subjectIDs = GetSubjectIDs(v);
            long[] periodPos = GetPeriodPos();

            double media1 = 0, media2 = 0;

            for (int i = 0; i < subjectIDs.Length; i++)
            {
                List<Grade> votiMateria = new List<Grade>(v.Grades.Where(s => s.SubjectId == subjectIDs[i]));
                List<Grade> votiPrimo = new List<Grade>(votiMateria.Where(s => s.PeriodPos == periodPos[0]));
                List<Grade> votiSecondo = new List<Grade>(votiMateria.Where(s => s.PeriodPos == periodPos[1]));

                if (votiPrimo.Count > 0)
                    pnlVotiPrimo.Dispatcher.Invoke(new Action(() => pnlVotiPrimo.Children.Add(new ControlMateria(votiPrimo) { Separator = i != 0 })));
                if (votiSecondo.Count > 0)
                    pnlVotiSecondo.Dispatcher.Invoke(new Action(() => pnlVotiSecondo.Children.Add(new ControlMateria(votiSecondo) { Separator = i != 0 })));

                media1 += Media(votiPrimo);
                media2 += Media(votiSecondo);
            }
            media1 /= subjectIDs.Length;
            media2 /= subjectIDs.Length;

            if (!double.IsNaN(media1))
            {
                ControlMateria mediaPrimoPeriodo = new ControlMateria("Media totale", media1);
                mediaPrimoPeriodo.SetValue(Grid.RowProperty, 1);
                GridPrimoPeriodo.Children.Add(mediaPrimoPeriodo);
            }

            if (!double.IsNaN(media2))
            {
                ControlMateria mediaSecondoPeriodo = new ControlMateria("Media totale", media2);
                mediaSecondoPeriodo.SetValue(Grid.RowProperty, 1);
                GridSecondoPeriodo.Children.Add(mediaSecondoPeriodo);
            }

            grpVoti.Dispatcher.Invoke(new Action(() => grpVoti.Visibility = Visibility.Visible));
        }

        private double Media(List<Grade> votiPrimo)
        {
            double count = 0, sum = 0;
            foreach (var item in votiPrimo.Where((s => s.DecimalValue.HasValue)))
            {
                count++;
                sum += item.DecimalValue.Value;
            }
            return sum / count;
        }

        private static long[] GetPeriodPos()
        {
            Data d = Data.instance;

            if (d.ValidLogin())
            {
                int sid = int.Parse(Regex.Replace(d.Login.Ident, "[^0-9.]", ""));

                IResponse response = API.Periodi(d.Login.Token, sid);
                if (response is Periodi)
                {
                    Periodi p = (Periodi)response;
                    long[] a = new long[p.Periods.Count];
                    for (int i = 0; i < a.Length; i++)
                        a[i] = p.Periods[i].PeriodPos;
                    return a;
                }
                else throw new Exception();
            }
            else throw new Exception();
        }

        private long[] GetSubjectIDs(Voti v)
        {
            List<long> a = new List<long>();

            foreach (Grade item in v.Grades)
                if (!a.Contains(item.SubjectId))
                    a.Add(item.SubjectId);

            return a.ToArray();
        }

        #endregion

        #endregion

        #region ProgressChanged

        private void BWProfiloProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 1)
            {
                Card info = ((UInfo)e.UserState).Cards.First();
                //TODO: Sent info to SideMenu in WindowMain
                //lblStudente.Text = $"{info.FirstName} {info.LastName} - {info.SchName} {info.SchDedication}";
                //pnlProfilo.Visibility = Visibility.Visible;
            }
            else
                MessageBox.Show("Login non effettuato\nDebug info: " + ((Errore)e.UserState).Message,
                    "Errore di login", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void BWFileProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 1)
            {
                ((Didattica)e.UserState).Didacticts.ForEach((Didactict d) => {
                    var item = GetTreeView(d.TeacherName, null);//"teacher.png");
                    d.Folders.ForEach((Folder f) => {
                        var folder = GetTreeView(f.FolderName, "folder.png");
                        f.Contents.ForEach((Content c) =>
                        {
                            var file = GetTreeView(
                                c.ContentName,
                                c.ObjectType.Equals("file") ? "file.png" : "link.png",
                                c,
                                new MouseButtonEventHandler((object osender, MouseButtonEventArgs mea) => {
                                    DoWork(new DoWorkEventArgs(null), () => {
                                        TreeViewItem a = osender as TreeViewItem;
                                        if (a != null)
                                        {
                                            Content b = a.Tag as Content;
                                            if (b != null)
                                            {
                                                if (b.ObjectType.Equals("file"))
                                                {
                                                    if (MessageBox.Show("Scaricare il file?", "File didattica",
                                                        MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
                                                    {
                                                        BackgroundWorker fileDownloader = new BackgroundWorker();
                                                        fileDownloader.WorkerSupportsCancellation = true;
                                                        fileDownloader.DoWork += FileDownloaderDoWork;

                                                        MessageBoxResult mbrOpen = MessageBoxResult.No;
                                                        MessageBoxResult mbrSave = MessageBox.Show("Salvare il file (Si) o aprire il file (No)", "File didattica",
                                                        MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);

                                                        string fldr = System.IO.Path.GetTempPath();
                                                        if (mbrSave == MessageBoxResult.Yes)
                                                        {
                                                            var fbd = new System.Windows.Forms.FolderBrowserDialog();
                                                            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                                                                fldr = fbd.SelectedPath + "\\";
                                                            mbrOpen = MessageBox.Show("Aprire il file una volta scaricato?", "File didattica",
                                                                MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                                                        }

                                                        fileDownloader.RunWorkerAsync(new object[] { b, mbrSave, fldr, mbrOpen });
                                                    }
                                                }
                                                else
                                                {
                                                    if (MessageBox.Show("Aprire il browser?", "Link didattica",
                                                        MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
                                                    {
                                                        DoWork(new DoWorkEventArgs(null), () =>
                                                        {
                                                            Data data = Data.instance;
                                                            int sid = int.Parse(Regex.Replace(data.Login.Ident, "[^0-9.]", ""));

                                                            IResponse dItem = API.DidatticaGetLink(data.Login.Token, sid, b.ContentId);
                                                            var didatticaItem = dItem as DidatticaItem;
                                                            if (didatticaItem != null)
                                                                Process.Start(didatticaItem.Item.Link);
                                                        });
                                                    }
                                                }
                                            }
                                        }
                                    });
                                })
                            );
                            folder.Items.Add(file);
                        });
                        item.Items.Add(folder);
                    });
                    trvFiles.Dispatcher.Invoke(() => { trvFiles.Items.Add(item); });
                });
            }
            else
                MessageBox.Show("Login non effettuato\nDebug info: " + ((Errore)e.UserState).Message,
                    "Errore di login", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        #region Other methods
        private TreeViewItem GetTreeView(string text, string imagePath, object tag = null, MouseButtonEventHandler MouseDoubleClick = null)
        {
            TreeViewItem item = new TreeViewItem();
            item.IsExpanded = false;
            item.Tag = tag;

            if (MouseDoubleClick != null)
                item.MouseDoubleClick += MouseDoubleClick;

            // create stack panel
            StackPanel stack = new StackPanel();
            stack.Orientation = Orientation.Horizontal;

            // create Image
            if (imagePath != null)
            {
                Image image = new Image();
                image.Source = new BitmapImage
                    (new Uri("pack://application:,,/Images/" + imagePath));
                image.Width = 24;
                image.Height = 24;
                stack.Children.Add(image);
            }

            // Label
            Label lbl = new Label();
            lbl.Content = text;

            // Add into stack
            stack.Children.Add(lbl);

            // assign stack to header
            item.Header = stack;
            return item;
        }
        #endregion

        #endregion

        #endregion
    }
}
