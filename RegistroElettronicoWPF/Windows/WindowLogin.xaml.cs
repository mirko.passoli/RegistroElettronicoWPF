﻿using Classeviva;
using System.ComponentModel;
using System.Windows;

namespace RegistroElettronicoWPF
{
    /// <summary>
    /// Logica di interazione per WindowLogin.xaml
    /// </summary>
    public partial class WindowLogin : Window
    {
        public WindowLogin()
        {
            InitializeComponent();
        }

        private void Login(object sender, RoutedEventArgs e)
        {
            Data d = Data.instance;

            if (d == null)
                d = new Data();

            txtMail.Text = "mirko.passoli@gmail.com";
            txtPassword.Password = "PassoliMirko1";

            d.Mail = txtMail.Text;
            d.Password = txtPassword.Password;

            BackgroundWorker bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;
            bw.DoWork += Login;
            bw.ProgressChanged += LoginComplete;
            bw.RunWorkerAsync();
        }

        private void LoginComplete(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage == 1)
            {
                Data.instance.Login = (Login)e.UserState;
                Data.instance.SaveAsync();
                DialogResult = true;
                Close();
            }
            else
                MessageBox.Show("Login non effettuato\nDebug info: " + ((Errore)e.UserState).Message,
                    "Errore di login", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void Login(object sender, DoWorkEventArgs e)
        {
            try
            {
                IResponse response = API.Login(Data.instance.Mail, Data.instance.Password);
                ((BackgroundWorker)sender).ReportProgress(response is Login ? 1 : 0, response);
            }
            catch (System.Net.WebException)
            {
                MessageBox.Show("Login non effettuato, verifica la connessione a internet",
                    "Errore di login", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (Width <= 600)
                ColonnaCentrale.Width = new GridLength(296);
            else
                ColonnaCentrale.Width = new GridLength(2, GridUnitType.Star);
        }
    }
}
