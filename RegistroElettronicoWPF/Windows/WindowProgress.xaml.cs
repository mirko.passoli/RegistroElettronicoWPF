﻿using System.Windows;

namespace RegistroElettronicoWPF
{
    /// <summary>
    /// Logica di interazione per WindowProgress.xaml
    /// </summary>
    public partial class WindowProgress : Window
    {
        public string LabelText { get => LText.Text; set => LText.Text = value; }
        public double ProgressValue { get => Progress.Value; set { Progress.Value = value; Progress.Text = $"{value:f1}%"; } }

        public WindowProgress()
        {
            InitializeComponent();
        }
    }
}
