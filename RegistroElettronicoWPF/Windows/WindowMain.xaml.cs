﻿using System.Windows;

namespace RegistroElettronicoWPF
{
    /// <summary>
    /// Logica di interazione per WindowMain.xaml
    /// </summary>
    public partial class WindowMain : Window
    {
        public const bool OFFLINE = false;  
        
        public WindowMain()
        {
            if (Data.instance == null)
                new Data();

            if (!Data.instance.ValidLogin() && !OFFLINE)
            {
                Hide();
                WindowLogin login = new WindowLogin();
                login.ShowDialog();
                bool a = login.DialogResult.HasValue ? login.DialogResult.Value : false;

                if (!a && Application.Current != null)
                    Application.Current.Shutdown();
                else
                    Show();
            }

            InitializeComponent();

            FrameMain.Content = new PageHome();
        }

    }
}
