﻿using Classeviva;
using System.Windows;

namespace RegistroElettronicoWPF
{
    /// <summary>
    /// Logica di interazione per WindowDettagliMateria.xaml
    /// </summary>
    public partial class WindowDettagliMateria : Window
    {
        public WindowDettagliMateria(ControlMateria materia, string s)
        {
            InitializeComponent();

            if (!string.IsNullOrWhiteSpace(s))
                Title = s;

            GridMain.Children.Add(new ControlMateria(materia) { Separator = false });

            double[] c = new double[3];
            double[] m = new double[3];

            materia.Voti.ForEach((Grade g) =>
            {
                ControlVoto cv = new ControlVoto(g);
                Voti.Children.Add(cv);

                if (g.DecimalValue.HasValue)
                {
                    m[g.ComponentPos - 1] += g.DecimalValue.Value;
                    c[g.ComponentPos - 1] ++;
                }
            });

            if (c[0] != 0)
                prgMediaScritto.Voto = new Grade() { DecimalValue = m[0] / c[0], DisplayValue = $"{m[0] / c[0]:f2}", Color = Classeviva.Voti.GetColor(m[0] / c[0]) };

            if (c[1] != 0)
                prgMediaOrale.Voto = new Grade() { DecimalValue = m[1] / c[1], DisplayValue = $"{m[1] / c[1]:f2}", Color = Classeviva.Voti.GetColor(m[1] / c[1]) };

            if (c[2] != 0)
                prgMediaPratico.Voto = new Grade() { DecimalValue = m[2] / c[2], DisplayValue = $"{m[2] / c[2]:f2}", Color = Classeviva.Voti.GetColor(m[2] / c[2]) };
        }
    }
}
