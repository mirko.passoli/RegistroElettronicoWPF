﻿using System.Collections.ObjectModel;

namespace RegistroElettronicoMVVM.ViewModel
{
	public class DesignViewModelMenuList : ViewModelMenuList
    {
        public DesignViewModelMenuList Instance => new DesignViewModelMenuList();
                
        public DesignViewModelMenuList()
        {
            Items = new ObservableCollection<ViewModelMenuListItem>
            {
                new ViewModelMenuListItem
                {
                    Text = "Home",
                    Icon = MaterialDesignThemes.Wpf.PackIconKind.Home
                },
                new ViewModelMenuListItem
                {
                    Text = "Account",
                    Icon = MaterialDesignThemes.Wpf.PackIconKind.Account
                }
            };
        }
    }
}
