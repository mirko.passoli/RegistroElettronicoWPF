﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;

using RegistroElettronicoMVVM.Command;
using RegistroElettronicoMVVM.Model;

namespace RegistroElettronicoMVVM.ViewModel
{
	class ViewModelControlVoto : ViewModelBase
	{
		private ICommand _submitCommand;
		private ObservableCollection<Voto> _voti;
		private Voto _voto;
		
		public ICommand SubmitCommand {
			get
			{
				if (_submitCommand == null)
					_submitCommand = new RelayCommand(param => Voti.Add(Voto), o => !string.IsNullOrEmpty(Voto.Display));
				return _submitCommand;
			}
		}

		ObservableCollection<Voto> Voti
		{
			get { return _voti; }
			set { _voti = value; NotifyPropertyChanged(nameof(Voti)); }
		}

		public Voto Voto
		{
			get { return _voto; }
			set { _voto = value; NotifyPropertyChanged(nameof(Voto)); }
		}

		public ViewModelControlVoto()
		{
			Voto = new Voto();
			Voti = new ObservableCollection<Voto>();
			Voti.CollectionChanged += Voti_CollectionChanged;
		}

		private void Voti_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) => NotifyPropertyChanged(nameof(Voti));
	}
}
