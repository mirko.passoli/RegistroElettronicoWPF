﻿using System;
using System.Collections.ObjectModel;

namespace RegistroElettronicoMVVM.ViewModel
{
	public class ViewModelMenuList : ViewModelBase
    {
        private ObservableCollection<ViewModelMenuListItem> _items;
        private int _selectedIndex;
        private ViewModelMenuListItem _selectedItem;
        
        public Action<object> MenuSelectionChangedExecute { get; set; }
        
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set { _selectedIndex = value; NotifyPropertyChanged(nameof(SelectedIndex)); }
        }

        public ViewModelMenuListItem SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; NotifyPropertyChanged(nameof(Items)); }
        }

        public ObservableCollection<ViewModelMenuListItem> Items
        {
            get { return _items; }
            set { _items = value; _items.CollectionChanged += Items_CollectionChanged; NotifyPropertyChanged(nameof(Items)); }
        }

        public ViewModelMenuList()
        {
            Items = new ObservableCollection<ViewModelMenuListItem>();
            SelectedItem = new ViewModelMenuListItem();
        }

        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) => NotifyPropertyChanged(nameof(Items));
    }
}
