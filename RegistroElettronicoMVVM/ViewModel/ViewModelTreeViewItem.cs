﻿namespace RegistroElettronicoMVVM.ViewModel
{
	public class ViewModelTreeViewItem : ViewModelBase
    {
        private string _text;

        public string Text
        {
            get { return _text; }
            set { _text = value; NotifyPropertyChanged(nameof(Text)); }
        }

    }
}
