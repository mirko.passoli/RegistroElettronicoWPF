﻿using MaterialDesignThemes.Wpf;

namespace RegistroElettronicoMVVM.ViewModel
{
	public class ViewModelMenuListItem : ViewModelBase
    {
        private PackIconKind _icon;
        private bool _isSelected;
        private string _text;

        public string Text
        {
            get { return _text; }
            set { _text = value; NotifyPropertyChanged(nameof(Text)); }
        }


        public PackIconKind Icon
        {
            get { return _icon; }
            set { _icon = value; NotifyPropertyChanged(nameof(Icon)); }
        }
        
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; NotifyPropertyChanged(nameof(IsSelected)); }
        }
    }
}
