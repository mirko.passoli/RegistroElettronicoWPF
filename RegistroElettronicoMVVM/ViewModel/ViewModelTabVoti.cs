﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace RegistroElettronicoMVVM.ViewModel
{
	public class ViewModelTabVoti : ViewModelBase
    {
        private string _header;
        private ObservableCollection<ViewModelMateria> _listMaterie;
        private ViewModelMateria _selectedVMM;

        public string Header
        {
            get { return _header; }
            set { _header = value; NotifyPropertyChanged(nameof(Header)); }
        }
        
        public ObservableCollection<ViewModelMateria> ListMaterie
        {
            get { return _listMaterie; }
            set { _listMaterie = value; ListMaterie.CollectionChanged += ListMaterie_CollectionChanged; NotifyPropertyChanged(nameof(ListMaterie)); }
        }
        
        public ViewModelMateria SelectedViewModelMateria
        {
            get { return _selectedVMM; }
            set { _selectedVMM = value; NotifyPropertyChanged((nameof(SelectedViewModelMateria))); }
        }
        
        public ViewModelTabVoti()
        {
            ListMaterie = new ObservableCollection<ViewModelMateria>();
            SelectedViewModelMateria = new ViewModelMateria();
        }
        
        private void ListMaterie_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) => NotifyPropertyChanged(nameof(ListMaterie));

        public override string ToString() => Header;
    }
}
