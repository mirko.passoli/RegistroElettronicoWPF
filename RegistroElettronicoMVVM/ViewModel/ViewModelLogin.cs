﻿using System;
using System.Windows.Input;

using RegistroElettronicoMVVM.Command;

namespace RegistroElettronicoMVVM.ViewModel
{
	public class ViewModelLogin : ViewModelBase
    {
        private Action<object> _execute;
        private ICommand _loginCommand;
        private string _mail;
        private bool _valid;
               
        public Action<object> Execute
        {
            get { return _execute; }
            set { _execute = value; NotifyPropertyChanged(nameof(LoginCommand)); }
        }
        
        public ICommand LoginCommand
        {
            get
            {
                if (_loginCommand == null)
                    _loginCommand = new RelayCommand(_execute += (o) => { Valid = true; }, (o) => !string.IsNullOrWhiteSpace(Mail) && Execute != null);
                return _loginCommand;
            }
        }

        public string Mail
        {
            get { return _mail; }
            set { _mail = value; NotifyPropertyChanged(nameof(Mail)); }
        }

        public bool Valid
        {
            get { return _valid; }
            set { _valid = value; NotifyPropertyChanged(nameof(Valid)); }
        }
        
        public ViewModelLogin() { Valid = true; }
    }
}
