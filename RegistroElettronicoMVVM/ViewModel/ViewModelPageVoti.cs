﻿using System.Collections.ObjectModel;

namespace RegistroElettronicoMVVM.ViewModel
{
	public class ViewModelPageVoti : ViewModelBase
    {
        private ObservableCollection<ViewModelTabVoti> _items;
        
        public ObservableCollection<ViewModelTabVoti> Items
        {
            get { return _items; }
            set { _items = value; Items.CollectionChanged += Items_CollectionChanged; NotifyPropertyChanged(nameof(Items)); }
        }
        
        public ViewModelPageVoti()
        {
            Items = new ObservableCollection<ViewModelTabVoti>();
        }

        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) => NotifyPropertyChanged(nameof(Items));
    }
}