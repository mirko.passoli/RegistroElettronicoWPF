﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;
using System.Windows.Media;

using RegistroElettronicoMVVM.Command;
using RegistroElettronicoMVVM.Model;

namespace RegistroElettronicoMVVM.ViewModel
{
	public class ViewModelMateria : ViewModelBase
	{
		//TODO: Implementare insegnanti
		private ICommand _addVotoIpoteticoCommand;
		private Materia _materia;
		private Obiettivo _obiettivo;
		private int _selectedNuovoObiettivoIndex;
		private int _selectedNuovoVotoIpoteticoIndex;
		private ICommand _setObiettivoCommand;
		private ObservableCollection<Voto> _voti;
		private ObservableCollection<Voto> _votiIpotetici;

		#region Properties
		public ICommand AddVotoIpoteticoCommand
		{
			get
			{
				if (_addVotoIpoteticoCommand == null)
					_addVotoIpoteticoCommand = new RelayCommand(
						//Command
						param => VotiIpotetici.Add(new Voto() { Display = Voto.Voti[SelectedNuovoVotoIpoteticoIndex], Value = SelectedNuovoVotoIpoteticoIndex / 4.0 + 1.0, SubjectID = Materia.ID }),
						//Condition to run
						o => SelectedNuovoVotoIpoteticoIndex > -1 && SelectedNuovoVotoIpoteticoIndex < Voto.Voti.Length
					);
				return _addVotoIpoteticoCommand;
			}
		}

		public bool HasObjective => Obiettivo != null && Obiettivo.Description != null;

		public Materia Materia
		{
			get { return _materia; }
			set
			{
				_materia = value;
				if (_obiettivo == null || _obiettivo.IDMateria != _materia.ID)
					Obiettivo = new Obiettivo(_materia.ID);
				NotifyPropertyChanged(nameof(Materia));
			}
		}

		public SolidColorBrush MediaColor => GetVotoColor(MediaValue.Value);

		public SolidColorBrush MediaOraleColor => GetVotoColor(MediaOraleValue.Value);

		public SolidColorBrush MediaPraticoColor => GetVotoColor(MediaPraticoValue.Value);

		public SolidColorBrush MediaScrittoColor => GetVotoColor(MediaScrittoValue.Value);

		public string MediaIpoteticaText => $"{MediaIpoteticaValue:f2}";

		public double? MediaIpoteticaValue
		{
			get
			{
				List<Voto> vo = new List<Voto>();
				if (Voti != null)
					vo.AddRange(Voti);
				if (VotiIpotetici != null)
					vo.AddRange(VotiIpotetici);
				return Media(new ObservableCollection<Voto>(vo));
			}
		}

		public double? MediaIpoteticaValue10 => MediaIpoteticaValue * 10;

		public string MediaIpoteticaVariazione
		{
			get
			{
				double? v = (MediaIpoteticaValue * 100 / MediaValue) - 100;
				if (double.IsNaN(v.Value))
					return string.Empty;
				if (v.Value >= 0)
					return $"+{v.Value:f2}%";
				else
					return $"{v.Value:f2}%";
			}
		}

		public string MediaOraleText => $"{MediaOraleValue:f2}";

		public double? MediaOraleValue => Media(Voti, 2);

		public double? MediaOraleValue10 => MediaOraleValue * 10;

		public string MediaPraticoText => $"{MediaPraticoValue.Value:f2}";

		public double? MediaPraticoValue => Media(Voti, 3);

		public double? MediaPraticoValue10 => MediaPraticoValue * 10;

		public string MediaScrittoText => $"{MediaScrittoValue:f2}";

		public double? MediaScrittoValue => Media(Voti, 1);

		public double? MediaScrittoValue10 => MediaScrittoValue * 10;

		public string MediaText => $"{MediaValue:f2}";

		public double? MediaValue => Media(Voti);

		public double? MediaValue10 => MediaValue * 10;

		public Obiettivo Obiettivo { get => _obiettivo; set { _obiettivo = value; NotifyPropertyChanged(nameof(Obiettivo)); } }
		
		public int SelectedNuovoObiettivoIndex { get => _selectedNuovoObiettivoIndex; set { _selectedNuovoObiettivoIndex = value; NotifyPropertyChanged(nameof(SelectedNuovoObiettivoIndex)); } }

		public int SelectedNuovoVotoIpoteticoIndex { get => _selectedNuovoVotoIpoteticoIndex; set { _selectedNuovoVotoIpoteticoIndex = value; NotifyPropertyChanged(nameof(SelectedNuovoVotoIpoteticoIndex)); } }

		public ICommand SetObiettivoCommand
		{
			get
			{
				if (_setObiettivoCommand == null)
					_setObiettivoCommand = new RelayCommand(
						SetObiettivo,
						o => SelectedNuovoVotoIpoteticoIndex > -1 && SelectedNuovoVotoIpoteticoIndex < Voto.Voti.Length
					);
				return _setObiettivoCommand;
			}
		}

		public ObservableCollection<Voto> Voti
		{
			get { return _voti; }
			set { _voti = value; Voti.CollectionChanged += Voti_CollectionChanged; NotifyPropertyChanged(nameof(MediaIpoteticaText)); NotifyPropertyChanged(nameof(MediaIpoteticaValue)); NotifyPropertyChanged(nameof(MediaIpoteticaValue10)); NotifyPropertyChanged(nameof(MediaOraleText)); NotifyPropertyChanged(nameof(MediaOraleValue)); NotifyPropertyChanged(nameof(MediaOraleValue10)); NotifyPropertyChanged(nameof(MediaPraticoText)); NotifyPropertyChanged(nameof(MediaPraticoValue)); NotifyPropertyChanged(nameof(MediaPraticoValue10)); NotifyPropertyChanged(nameof(MediaScrittoText)); NotifyPropertyChanged(nameof(MediaScrittoValue)); NotifyPropertyChanged(nameof(MediaScrittoValue10)); NotifyPropertyChanged(nameof(MediaText)); NotifyPropertyChanged(nameof(MediaValue)); NotifyPropertyChanged(nameof(MediaValue10)); NotifyPropertyChanged(nameof(MediaIpoteticaVariazione)); NotifyPropertyChanged(nameof(Voti)); }
		}

		public ObservableCollection<Voto> VotiIpotetici
		{
			get { return _votiIpotetici; }
			set { _votiIpotetici = value; VotiIpotetici.CollectionChanged += VotiIpotetici_CollectionChanged; NotifyPropertyChanged(nameof(VotiIpotetici)); NotifyPropertyChanged(nameof(MediaIpoteticaText)); NotifyPropertyChanged(nameof(MediaIpoteticaValue)); NotifyPropertyChanged(nameof(MediaIpoteticaVariazione)); NotifyPropertyChanged(nameof(MediaIpoteticaValue10)); }
		}
		#endregion

		public ViewModelMateria()
		{
			Voti = new ObservableCollection<Voto>();
			VotiIpotetici = new ObservableCollection<Voto>();

			SelectedNuovoVotoIpoteticoIndex = 20;
			SelectedNuovoObiettivoIndex = 20;
		}

		private void VotiIpotetici_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			NotifyPropertyChanged(nameof(MediaIpoteticaText)); NotifyPropertyChanged(nameof(MediaIpoteticaValue)); NotifyPropertyChanged(nameof(MediaIpoteticaValue10)); NotifyPropertyChanged(nameof(MediaIpoteticaVariazione)); NotifyPropertyChanged(nameof(Voti));
		}

		private void Voti_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			NotifyPropertyChanged(nameof(MediaIpoteticaText)); NotifyPropertyChanged(nameof(MediaIpoteticaValue)); NotifyPropertyChanged(nameof(MediaIpoteticaValue10)); NotifyPropertyChanged(nameof(MediaOraleText)); NotifyPropertyChanged(nameof(MediaOraleValue)); NotifyPropertyChanged(nameof(MediaOraleValue10)); NotifyPropertyChanged(nameof(MediaPraticoText)); NotifyPropertyChanged(nameof(MediaPraticoValue)); NotifyPropertyChanged(nameof(MediaPraticoValue10)); NotifyPropertyChanged(nameof(MediaScrittoText)); NotifyPropertyChanged(nameof(MediaScrittoValue)); NotifyPropertyChanged(nameof(MediaScrittoValue10)); NotifyPropertyChanged(nameof(MediaText)); NotifyPropertyChanged(nameof(MediaValue)); NotifyPropertyChanged(nameof(MediaValue10)); NotifyPropertyChanged(nameof(MediaIpoteticaVariazione)); NotifyPropertyChanged(nameof(Voti));
		}

		private SolidColorBrush GetVotoColor(double media)
		{
			switch (Classeviva.Voti.GetColor(media))
			{
				case Classeviva.Color.Blue:
					return Brushes.Blue;
				case Classeviva.Color.Green:
					return Brushes.Green;
				case Classeviva.Color.Red:
					return Brushes.Red;
				default:
					return null;
			}
		}

		private double? Media(ObservableCollection<Voto> voti)
		{
			if (voti == null)
				return 0;

			double c = 0, v = 0;
			
			foreach (var item in voti)
			{
				if (item.Value.HasValue)
				{
					v += item.Value.Value;
					c++;
				}
			}
			return v / c;
		}

		private double? Media(ObservableCollection<Voto> voti, int typePos)
		{
			if (voti == null)
				return 0;

			double c = 0, v = 0;
			foreach (var item in voti)
			{
				if (item.Value.HasValue && item.TypePosition == typePos)
				{
					v += item.Value.Value;
					c++;
				}
			}
			return v / c;
		}

		private void SetObiettivo(object o)
		{
			Obiettivo.Value = SelectedNuovoObiettivoIndex / 4.0 + 1.0;
			Obiettivo.Text = Voto.Voti[SelectedNuovoObiettivoIndex];

			int count;
			double d = 0, somma = Somma(Voti, out count);
			d = (Obiettivo.Value * (count + 1)) - somma;

			if (d < 1)
				Obiettivo.Description = "Puoi stare tranquillo";
			else if (d > 10)
				Obiettivo.Description = "Obiettivo irraggiungibile";
			else if (d > Obiettivo.Value)
				Obiettivo.Description = $"Devi prendere almeno {d:f2}";
			else
				Obiettivo.Description = $"Non prendere meno di {d:f2}";

			NotifyPropertyChanged(nameof(Materia));
			NotifyPropertyChanged(nameof(HasObjective));
		}

		private double Somma(ObservableCollection<Voto> voti, out int count)
		{
			count = 0;
			if (voti == null)
				return 0;
			
			double v = 0;
			foreach (var item in voti)
				if (item.Value.HasValue)
				{
					v += item.Value.Value;
					count++;
				}

			return v;
		}
	}
}
