﻿using System;
using System.Windows.Input;

using RegistroElettronicoMVVM.Command;
using RegistroElettronicoMVVM.View;

namespace RegistroElettronicoMVVM.ViewModel
{
	class ViewModelMainWindow : ViewModelBase
    {
        private bool _canOpenDrawer;
        private bool _isDrawerOpen;
        private Action<object> _logoutAcion;
        private ICommand _logoutCommand;
        private bool _openDrawer;
        private IPage _pageContent;
        private string _title = "Registro Elettronico";
        private ViewModelLogin _vml;
        private ViewModelMenuList _vmml;
        private ViewModelPageVoti _vmpl;

        public bool CanOpenDrawer
        {
            get { return _canOpenDrawer; }
            set { _canOpenDrawer = value; NotifyPropertyChanged(nameof(CanOpenDrawer)); NotifyPropertyChanged(nameof(IsDrawerOpen)); }
        }

        public bool IsDrawerOpen
        {
            get
            {
                _isDrawerOpen = _openDrawer && _canOpenDrawer;
                if (_isDrawerOpen)
                    _openDrawer = false;
                return _isDrawerOpen;
            }
        }

        public Action<object> LogoutAction
        {
            get { return _logoutAcion; }
            set { _logoutAcion = value; _logoutCommand = null; NotifyPropertyChanged(nameof(LogoutCommand)); }
        }

        public ICommand LogoutCommand
        {
            get
            {
                if (_logoutCommand == null)
                    _logoutCommand = new RelayCommand(LogoutAction, (o) => LogoutAction != null);
                return _logoutCommand;
            }
        }
        
        public bool OpenDrawer
        {
            get { return _openDrawer; }
            set { _openDrawer = value; NotifyPropertyChanged(nameof(OpenDrawer)); NotifyPropertyChanged(nameof(IsDrawerOpen)); }
        }

        public IPage PageContent
        {
            get { return _pageContent; }
            set { _pageContent = value; NotifyPropertyChanged(nameof(PageContent)); }
        }
        
        public string Title
        {
            get { return _title; }
            set { _title = value; NotifyPropertyChanged(nameof(Title)); }
        }

        public ViewModelLogin VMLogin
        {
            get { return _vml; }
            set { _vml = value; NotifyPropertyChanged(nameof(VMLogin)); }
        }

        public ViewModelMenuList VMMenuList
        {
            get { return _vmml; }
            set { _vmml = value; NotifyPropertyChanged(nameof(VMMenuList)); }
        }
        
        public ViewModelPageVoti VMPageVoti
        {
            get { return _vmpl; }
            set { _vmpl = value; NotifyPropertyChanged(nameof(VMPageVoti)); }
        }
    }
}
