﻿using System.Threading;

using RegistroElettronicoMVVM.View.Windows;
using RegistroElettronicoMVVM.ViewModel;

namespace RegistroElettronicoMVVM.Utils
{
	public class SplashScreenHelper
    {
        public static SplashWindow SplashScreen { get; set; }

        public static void Show()
        {
            if (SplashScreen != null)
                SplashScreen.Show();
        }

        public static void Hide()
        {
            if (SplashScreen == null) return;

            Thread t = new Thread(() =>
            {
                SplashScreen.Dispatcher.Invoke(() => SplashScreen.Hide());
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }

        internal static void Close()
        {
            if (SplashScreen == null) return;

            Thread t = new Thread(() =>
            {
                SplashScreen.Dispatcher.Invoke(() => SplashScreen.Close());
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }

        public static void ShowText(string text)
        {
            if (SplashScreen == null) return;

            Thread t = new Thread(() =>
            {
                SplashScreen.Dispatcher.Invoke(() => ((ViewModelSlashScreen)SplashScreen.DataContext).Text = text);
            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }
    }
}
