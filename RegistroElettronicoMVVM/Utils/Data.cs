﻿using System;
using System.Collections.Generic;

using Classeviva;

using Newtonsoft.Json;

using RegistroElettronicoMVVM.Model;
using RegistroElettronicoMVVM.Utils;

namespace RegistroElettronicoMVVM
{
	public class Data
    {
        private static Data _instance = null;

        private string _password;
        private List<DownloadDidattica> _downloadsDidattica;

        public Classeviva.Login Login { get; set; }
        public string Mail { get; set; }
        public string Password { get => StringCipher.Decrypt(_password, "RegistroElettronico"); set => _password = StringCipher.Encrypt(value, "RegistroElettronico"); }
        public string EncriptedPassword { get => _password; set => _password = value; }

        /// <summary>
        /// Ultima risposta dall'API in JSON - Voti
        /// </summary>
        public string LRVoti { get; set; }
        /// <summary>
        /// Ultima risposta dall'API in JSON - Didattica
        /// </summary>
        public string LRDidattica { get; set; }
        
        public List<DownloadDidattica> DownloadsDidattica
        {
            get
            {
                if (_downloadsDidattica == null)
                    _downloadsDidattica = new List<DownloadDidattica>();
                return _downloadsDidattica;
            }
        }

        public static Data Instance
        {
            get
            {
                if (_instance == null)
                    _instance = JsonConvert.DeserializeObject<Data>(Properties.Settings.Default.Data);
                if (_instance == null)
                    new Data();
                return _instance;
            }
        }
        
        public Data()
        {
            _instance = this;
        }
        
        public void Save()
        {
            Properties.Settings.Default.Data = JsonConvert.SerializeObject(this);
            Properties.Settings.Default.Save();
        }

        public void Delete()
        {
            Properties.Settings.Default.Data = JsonConvert.SerializeObject(null);
            Properties.Settings.Default.Save();
            _instance = null;
        }

        public bool ValidLogin() => Login != null && !string.IsNullOrEmpty(Login.Token) && DateTime.Parse(Login.Expire) > DateTime.Now;

        public bool ShouldSerializePassword() => false;
    }
}
