﻿using System;
using System.Threading;
using System.Windows.Controls;

using Classeviva;

using RegistroElettronicoMVVM.ViewModel;

namespace RegistroElettronicoMVVM.View.Pages
{
	/// <summary>
	/// Interaction logic for PageLogin.xaml
	/// </summary>
	public partial class PageLogin : Page, IPage
    {
        ViewModelLogin _vml;

        public PageLogin(ViewModelLogin vml = null, Action success = null, Action error = null, bool autologin = false)
        {
            if (autologin)
            {
                if (Data.Instance.Login == null)
                    Login(Data.Instance);
                if (Data.Instance.ValidLogin())
                {
                    success?.Invoke();
                    return;
                } 
            }

            if (vml == null)
                vml = new ViewModelLogin();

            _vml = vml;

            _vml.Execute = (o) => new Thread(() => ButtonCode(success, error)).Start();
            DataContext = _vml;

            InitializeComponent();
        }
        
        private void ButtonCode(Action success, Action error)
        {
            Data.Instance.Mail = _vml.Mail;
            Data.Instance.Password = PasswordBox.Password;
            try
            {
                IResponse response = API.Login(Data.Instance.Mail, Data.Instance.Password);
                if (response is Login l)
                {
                    _vml.Valid = true;
                    Data.Instance.Login = l;
                    success?.Invoke();
                }
                else
                {
                    _vml.Valid = false;
                    error?.Invoke();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                _vml.Valid = false;
                error?.Invoke();
            }
        }

        private static void Login(Data d)
        {
            if (d.Mail != null && d.Password != null)
            {
                IResponse apiresponse = API.Login(d.Mail, d.Password);
                if (apiresponse is Login)
                {
                    d.Login = (Login)apiresponse;
                    d.Save();
                }
            }
        }

        public void OnClosing() { }
    }
}
