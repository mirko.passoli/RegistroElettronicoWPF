﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using Classeviva;

using MaterialDesignThemes.Wpf;

namespace RegistroElettronicoMVVM.View.Pages
{
	/// <summary>
	/// Logica di interazione per PageDidattica.xaml
	/// </summary>
	public partial class PageDidattica : Page, IPage
	{
		private List<Thread> _threads;
		private Action _requestLogin;

		private string DialogTitle { get => txtDTitle.Text; set => txtDTitle.Text = value; }
		private string DialogContent { get => txtDContent.Text; set => txtDContent.Text = value; }
		
		public PageDidattica(Action requestLogin = null)
		{
			if (requestLogin == null)
				requestLogin = MainWindow.Instance.RequestLogin;
			_requestLogin = requestLogin;

			InitializeComponent();

			_threads = new List<Thread>();

			Thread threadInit = new Thread(Init);
			threadInit.SetApartmentState(ApartmentState.STA);
			_threads.Add(threadInit);

			threadInit.Start();
		}

		public void OnClosing()
		{
			foreach (var item in _threads)
				if (item != null && item.IsAlive)
					item.Abort(0);
		}

		private void Init()
		{
			Data d = Data.Instance;
			int sid = d.Login.GetSID();

			bool logged = TryLogin();

			if (logged)
			{
				IResponse response = API.Didattica(d.Login.Token, sid);
				if (response is Didattica didattica)
				{
					treeView.Dispatcher.Invoke(() => 
					{
						foreach (var didactict in didattica.Didacticts)
					{
						var item = GetTreeViewItem(didactict.TeacherName, "user.png");
						foreach (var folder in didactict.Folders)
						{
							var folderItem = GetTreeViewItem(folder.FolderName, "folder.png");
							foreach (var content in folder.Contents)
							{
								var fileItem = GetTreeViewItem(content.ContentName,
									content.ObjectType.Equals("file") ? "file.png" : "link.png", content);
								fileItem.MouseDoubleClick += FileTreeViewItem_MouseDown;
								folderItem.Items.Add(fileItem);
							}
							item.Items.Add(folderItem);
						}
						
						treeView.Items.Add(item);
					}
						label.Visibility = Visibility.Collapsed;
					});
				}
			}
			else _requestLogin?.Invoke();
		}

		private void FileTreeViewItem_MouseDown(object sender, MouseButtonEventArgs e)
		{
			bool logged = TryLogin();

			if (logged)
			{
				if (sender is TreeViewItem item)
				{
					if (item.Tag is Content c)
					{
						if (c.ObjectType.Equals("file"))
						{
							Data data = Data.Instance;
							foreach (var downloadedfile in data.DownloadsDidattica.Where((d)=>d.ContentId == c.ContentId))
							{
								if (File.Exists(downloadedfile.FilePath))
								{
									Dispatcher.Invoke(() => Snackbar.MessageQueue.Enqueue("File già scaricato, apertura in corso"));
									Process.Start(downloadedfile.FilePath);
									return;
								}
							}
							
							DialogTitle = "Download file";
							DialogContent = "Scaricare il file " + c.ContentName + "?";
							DDummy.Tag = c;

							DialogHost.OpenDialogCommand.Execute(null, null);
						}
						else
						{
							if (MessageBox.Show("Aprire il browser?", "Link didattica",
								MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes)
							{
								if (TryLogin())
								{
									Data data = Data.Instance;
									int sid = data.Login.GetSID();

									IResponse dItem = API.DidatticaGetLink(data.Login.Token, sid, c.ContentId);
									var didatticaItem = dItem as DidatticaItem;
									if (didatticaItem != null)
										Process.Start(didatticaItem.Item.Link);
								}
							}
						}
					}
				}
			}
			else _requestLogin?.Invoke();
		}

		private bool TryLogin()
		{
			bool logged = false;

			Data d = Data.Instance;
			for (int i = 0; i < 5 && !logged; i++)
			{
				if (!(logged = d.ValidLogin()))
				{
					IResponse apiresponse = API.Login(d.Mail, d.Password);
					if (apiresponse is Login)
					{
						d.Login = (Login)apiresponse;
						d.Save();
					}
				}
			}

			return logged;
		}

		private TreeViewItem GetTreeViewItem(string text = null, string imageName = null, object tag = null)
		{
			TreeViewItem item = new TreeViewItem();
			item.IsExpanded = false;
			item.Tag = tag;

			StackPanel stack = new StackPanel();
			stack.Orientation = Orientation.Horizontal;

			// create Image
			if (imageName != null)
			{
				try
				{
					Image image = new Image();
					//image.Source = new BitmapImage
						//(new Uri("pack://application:,,/Images/" + imageName));
					image.Width = 24;
					image.Height = 24;
					stack.Children.Add(image);
				}
				catch (Exception)
				{
				}
			}

			// Label
			Label lbl = new Label();
			lbl.Content = text;

			// Add into stack
			stack.Children.Add(lbl);

			// assign stack to header
			item.Header = stack;
			return item;
		}

		private void DialogHost_DialogClosing(object sender, DialogClosingEventArgs eventArgs)
		{
			if ((eventArgs.Parameter as string).Equals("Annulla"))
				Console.WriteLine("Annulla");

			if (DDummy.Tag is Content c)
			{
				string path;

				switch (eventArgs.Parameter as string)
				{
					case "Apri":
						path = Path.GetTempPath() + "Registro Elettronico\\";
						break;
					case "Salva":
						var fbd = new System.Windows.Forms.FolderBrowserDialog();
						if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
							path = fbd.SelectedPath + "\\";
						else path = Path.GetTempPath() + "Registro Elettronico\\";
						break;
					default:
						return;
				}

				Thread t = new Thread(() =>
				{
					bool logged = TryLogin();

					if (logged)
					 {
						Data data = Data.Instance;
						int sid = data.Login.GetSID();

						Dispatcher.Invoke(() => Snackbar.MessageQueue.Enqueue("Inizio download in " + path));
						string file = API.DidatticaDownloadFile(data.Login.Token, sid, c.ContentId, path);
						Dispatcher.Invoke(() => Snackbar.MessageQueue.Enqueue(
							"Download conmpletato",
							"APRI",
							() => Process.Start(file)
						));

						data.DownloadsDidattica.Add(new Model.DownloadDidattica { ContentId = c.ContentId, FilePath = file });
						data.Save();
					}
					else MessageBox.Show(
						"Errore, ripetere il login!", "Errore download file didattica", MessageBoxButton.OK, MessageBoxImage.Error);
				});
				t.SetApartmentState(ApartmentState.STA);
				_threads.Add(t);

				t.Start();
			}
		}
	}
}
