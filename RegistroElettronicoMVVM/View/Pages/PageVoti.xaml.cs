﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

using Classeviva;

using Newtonsoft.Json;

using RegistroElettronicoMVVM.Model;
using RegistroElettronicoMVVM.ViewModel;

namespace RegistroElettronicoMVVM.View
{
	/// <summary>
	/// Logica di interazione per PageVoti.xaml
	/// </summary>
	public partial class PageVoti : Page, IPage
	{
		private List<Thread> _threads;

		private ViewModelPageVoti _vmpv = null;

		public PageVoti(ViewModelPageVoti vmpv = null, Action requestLogin = null)
		{
			_threads = new List<Thread>();

			if (requestLogin == null)
			{
				requestLogin = MainWindow.Instance.RequestLogin;
			}

			InitializeComponent();

			if (vmpv == null)
			{
				vmpv = new ViewModelPageVoti();
				vmpv.Items = new ObservableCollection<ViewModelTabVoti> { new ViewModelTabVoti() { Header = "PRIMO PERIODO" }, new ViewModelTabVoti() { Header = "SECONDO PERIODO" } };
			}

			_vmpv = vmpv;
			DataContext = vmpv;

			new Thread(() => Init(vmpv, requestLogin)).Start();
		}

		private void Init(ViewModelPageVoti vmpv, Action requestLogin)
		{
			List<Materia> lMaterie = new List<Materia>();
			List<Voto> lVotiPrimoPeriodo = new List<Voto>();
			List<Voto> lVotiSecondoPeriodo = new List<Voto>();

			//Materie
			Thread materie = new Thread(() =>
			{
				try
				{
					Data d = Data.Instance;
					for (int i = 0; i < 5 && !d.ValidLogin(); i++)
					{
						Login(d);
					}
					if (!d.ValidLogin())
					{
						requestLogin?.Invoke();
					}

					int sid = d.Login.GetSID();
					IResponse res = API.Materie(d.Login.Token, sid);

					if (res is Materie m)
					{
						foreach (Subject item in m.Subjects)
						{
							lMaterie.Add(new Materia(item));
						}
					}
				}
				catch (Exception ex)
				{
					Utils.SplashScreenHelper.Close();
					MessageBox.Show($"{ex.Message}\n\nDebug Info:\nError: {ex.StackTrace.Substring(ex.StackTrace.LastIndexOf("in"))}");
					throw;
				}
			});

			//Voti
			Thread voti = new Thread(() =>
			{
				try
				{
					Data d = Data.Instance;
					for (int i = 0; i < 5 && !d.ValidLogin(); i++)
					{
						Login(d);
					}
					if (!d.ValidLogin())
					{
						requestLogin?.Invoke();
					}

					int sid = d.Login.GetSID();
					IResponse res = API.Voti(d.Login.Token, sid);

					if (res is Voti v)
					{
						foreach (Grade item in v.Grades)
						{
							if (item.PeriodPos == 1)
							{
								lVotiPrimoPeriodo.Add(new Voto(item));
							}
							else
							{
								lVotiSecondoPeriodo.Add(new Voto(item));
							}
						}
					}
				}
				catch (Exception ex)
				{
					Utils.SplashScreenHelper.Close();
					MessageBox.Show($"{ex.Message}\n\nDebug Info:\nError: {ex.StackTrace.Substring(ex.StackTrace.LastIndexOf("in"))}");
					throw;
				}
			});

			materie.Start();
			voti.Start();

			materie.Join();
			voti.Join();

			//From JSON
			List<Obiettivo> lObiettivi1 = JsonConvert.DeserializeObject<List<Obiettivo>>(Properties.Settings.Default.Obiettivi1);
			List<Voto> lVotiI1 = JsonConvert.DeserializeObject<List<Voto>>(Properties.Settings.Default.VotiI1);

			List<Obiettivo> lObiettivi2 = JsonConvert.DeserializeObject<List<Obiettivo>>(Properties.Settings.Default.Obiettivi2);
			List<Voto> lVotiI2 = JsonConvert.DeserializeObject<List<Voto>>(Properties.Settings.Default.VotiI2);

			Dispatcher.Invoke(() => vmpv.Items[0].ListMaterie.Clear());
			Dispatcher.Invoke(() => vmpv.Items[1].ListMaterie.Clear());

			lMaterie.ForEach((Materia m) =>
			{
				//Primo periodo                
				ViewModelMateria v1 = new ViewModelMateria
				{
					Materia = m,
					Voti = new ObservableCollection<Voto>(lVotiPrimoPeriodo.Where((v) => v.SubjectID == m.ID))
				};

				if (lObiettivi1 != null)
				{
					List<Obiettivo> l1 = new List<Obiettivo>(lObiettivi1.Where((o) =>
					{
						if (o == null)
						{
							return false;
						}

						return o.IDMateria == m.ID;
					}));
					if (l1.Count > 0)
					{
						v1.Obiettivo = l1.First();
					}
				}

				if (lVotiI1 != null)
				{
					List<Voto> l1 = new List<Voto>(lVotiI1.Where((o) =>
					{
						if (o == null)
						{
							return false;
						}

						return o.SubjectID == m.ID;
					}));
					v1.VotiIpotetici = new ObservableCollection<Voto>(l1);
				}

				Dispatcher.Invoke(() => vmpv.Items[0].ListMaterie.Add(v1));

				//Secondo periodo
				ViewModelMateria v2 = new ViewModelMateria
				{
					Materia = m,
					Voti = new ObservableCollection<Voto>(lVotiSecondoPeriodo.Where((v) => v.SubjectID == m.ID))
				};

				if (lObiettivi2 != null)
				{
					List<Obiettivo> l2 = new List<Obiettivo>(lObiettivi2.Where((o) =>
					{
						if (o == null)
						{
							return false;
						}

						return o.IDMateria == m.ID;
					}));
					if (l2.Count > 0)
					{
						v2.Obiettivo = l2.First();
					}
				}
				if (lVotiI2 != null)
				{
					List<Voto> l2 = new List<Voto>(lVotiI2.Where((o) =>
					{
						if (o == null)
						{
							return false;
						}

						return o.SubjectID == m.ID;
					}));
					v2.VotiIpotetici = new ObservableCollection<Voto>(l2);
				}

				Dispatcher.Invoke(() => vmpv.Items[1].ListMaterie.Add(v2));
			});

			Dispatcher.Invoke(() =>
			{
				vmpv.Items[0].SelectedViewModelMateria = vmpv.Items[0].ListMaterie[0];
				vmpv.Items[1].SelectedViewModelMateria = vmpv.Items[1].ListMaterie[0];
				labelCaricamento.Visibility = Visibility.Collapsed;
				tab.Visibility = Visibility.Visible;
			});
		}

		private static void Login(Data d)
		{
			if (d.Mail != null && d.Password != null)
			{
				IResponse apiresponse = API.Login(d.Mail, d.Password);
				if (apiresponse is Classeviva.Login)
				{
					d.Login = (Classeviva.Login)apiresponse;
					d.Save();
				}
			}
		}

		public void OnClosing()
		{
			for (int i = 0; i < 2; i++)
			{
				List<Obiettivo> lobiettivo = new List<Obiettivo>();
				List<Voto> lvotii = new List<Voto>();
				foreach (ViewModelMateria item in _vmpv.Items[i].ListMaterie)
				{
					lobiettivo.Add(item.Obiettivo);
					foreach (Voto vi in item.VotiIpotetici)
					{
						lvotii.Add(vi);
					}
				}
				if (i == 0)
				{
					Properties.Settings.Default.Obiettivi1 = JsonConvert.SerializeObject(lobiettivo);
					Properties.Settings.Default.VotiI1 = JsonConvert.SerializeObject(lvotii);
				}
				else
				{
					Properties.Settings.Default.Obiettivi2 = JsonConvert.SerializeObject(lobiettivo);
					Properties.Settings.Default.VotiI2 = JsonConvert.SerializeObject(lvotii);
				}
			}
			Properties.Settings.Default.Save();
		}
	}
}
