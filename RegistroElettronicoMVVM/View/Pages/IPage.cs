﻿namespace RegistroElettronicoMVVM.View
{
	public interface IPage
    {
        void OnClosing();
    }
}