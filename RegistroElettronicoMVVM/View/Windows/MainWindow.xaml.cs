﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

using Classeviva;

using Newtonsoft.Json;

using RegistroElettronicoMVVM.View;
using RegistroElettronicoMVVM.View.Pages;
using RegistroElettronicoMVVM.ViewModel;

namespace RegistroElettronicoMVVM
{
	/// <summary>
	/// Logica di interazione per MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private Thread _threadCheck;
		private ViewModelMainWindow _vmmw;

		public static MainWindow Instance { get; set; }

		public MainWindow()
		{
			Instance = this;
			InitializeComponent();

			_vmmw = new ViewModelMainWindow();
			_vmmw.VMLogin = new ViewModelLogin();
			_vmmw.VMMenuList = new ViewModelMenuList { MenuSelectionChangedExecute = MenuSelectionChanged };
			_vmmw.VMPageVoti = new ViewModelPageVoti
			{
				Items = new ObservableCollection<ViewModelTabVoti> { new ViewModelTabVoti() { Header = "PRIMO PERIODO" }, new ViewModelTabVoti() { Header = "SECONDO PERIODO" } }
			};
			_vmmw.PageContent = new PageLogin(_vmmw.VMLogin, LoginSuccess, autologin: true);

			_vmmw.LogoutAction = Logout;                      

			DataContext = _vmmw;

			_threadCheck = new Thread(async () => await CheckAsync());
			System.Timers.Timer timer = new System.Timers.Timer(1800000);
			timer.Elapsed += (o, e) => { _threadCheck.Start(); };
			timer.Start();
		}

		private async Task CheckAsync()
		{
			Data d = Data.Instance;
			int sid = d.Login.GetSID();

			string prevDidattica = d.LRDidattica;
			string prevVoti = d.LRVoti;

			IResponse didattica = API.Didattica(d.Login.Token, sid);
			IResponse voti = API.Voti(d.Login.Token, sid);

			if (didattica is Didattica)
				d.LRDidattica = JsonConvert.SerializeObject(didattica);

			if (voti is Voti)
				d.LRVoti = JsonConvert.SerializeObject(API.Voti(d.Login.Token, sid));

			System.Console.WriteLine(d.LRDidattica);
			System.Console.WriteLine(d.LRVoti);

			if (!string.IsNullOrWhiteSpace(prevDidattica) && !prevDidattica.Equals(d.LRDidattica))
				await App.ShowToastAsync("Registro Elettronico", "Nuovo elemento in didattica");

			if (!string.IsNullOrWhiteSpace(prevVoti) && !prevDidattica.Equals(d.LRVoti))
				await App.ShowToastAsync("Registro Elettronico", "Nuovo voto pubblicato");
		}

		private void LoginSuccess()
		{
			Data.Instance.Save();
			Dispatcher.Invoke(() =>
			{
				_vmmw.VMMenuList.Items.Add(new ViewModelMenuListItem { Text = "Voti", Icon = MaterialDesignThemes.Wpf.PackIconKind.TrendingUp });
				_vmmw.VMMenuList.Items.Add(new ViewModelMenuListItem { Text = "Didattica", Icon = MaterialDesignThemes.Wpf.PackIconKind.Folder });
				_vmmw.CanOpenDrawer = true;
				_vmmw.VMMenuList.SelectedIndex = 0;
			});
		}

		private void Logout(object o)
		{
			Data.Instance.Delete();
			_vmmw.CanOpenDrawer = false;
			_vmmw.VMMenuList.Items = new ObservableCollection<ViewModelMenuListItem>();
			_vmmw.PageContent = new PageLogin(_vmmw.VMLogin, LoginSuccess);
		}

		private void MenuSelectionChanged(object o)
		{
			_vmmw.PageContent.OnClosing();

			switch (_vmmw.VMMenuList.SelectedIndex)
			{
				case 0:
					_vmmw.PageContent = new PageVoti(_vmmw.VMPageVoti, RequestLogin);
					break;
				case 1:
					_vmmw.PageContent = new PageDidattica();
					break;
				default:
					break;
			}
			if (_vmmw.VMMenuList.SelectedItem == null)
				_vmmw.Title = "Registro Elettronico";
			else
				_vmmw.Title = _vmmw.VMMenuList.SelectedItem.Text;
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			_vmmw.PageContent.OnClosing();
		}

		public void RequestLogin()
		{
			Dispatcher.Invoke(() =>
			{
				_vmmw.PageContent = new PageLogin(_vmmw.VMLogin, LoginSuccess, autologin: true);
				_vmmw.CanOpenDrawer = false;
			});
		}
	}
}
