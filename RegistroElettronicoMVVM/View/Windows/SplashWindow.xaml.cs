﻿using System.Windows;

using RegistroElettronicoMVVM.ViewModel;

namespace RegistroElettronicoMVVM.View.Windows
{
	/// <summary>
	/// Interaction logic for SplashWindow.xaml
	/// </summary>
	public partial class SplashWindow : Window
    {
        ViewModelSlashScreen _vm = new ViewModelSlashScreen();
        public SplashWindow()
        {
            InitializeComponent();
            DataContext = _vm;
        }
    }
}
