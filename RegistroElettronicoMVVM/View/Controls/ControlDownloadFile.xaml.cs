﻿using System.Windows.Controls;

namespace RegistroElettronicoMVVM.View.Controls
{
	/// <summary>
	/// Logica di interazione per ControlDownloadFile.xaml
	/// </summary>
	public partial class ControlDownloadFile : UserControl
    {
        public string Text { get => txtText.Text; set => txtText.Text = value; }
        public double Value { get => prgValue.Value; set => prgValue.Value = value; }

        public ControlDownloadFile()
        {
            InitializeComponent();
        }
    }
}
