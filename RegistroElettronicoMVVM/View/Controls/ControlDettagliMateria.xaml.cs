﻿using System.Collections.ObjectModel;
using System.Windows.Controls;

using RegistroElettronicoMVVM.Model;

namespace RegistroElettronicoMVVM.View.Controls
{
	/// <summary>
	/// Interaction logic for ControlDettagliMateria.xaml
	/// </summary>
	public partial class ControlDettagliMateria : UserControl
	{
		public ControlDettagliMateria()
		{
			InitializeComponent();
		}

		private void ListViewItem_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (lvVotiIpotetici.ItemsSource is ObservableCollection<Voto> voti)
				voti.Remove((Voto)((ListBoxItem)sender).Content);
		}
	}
}
