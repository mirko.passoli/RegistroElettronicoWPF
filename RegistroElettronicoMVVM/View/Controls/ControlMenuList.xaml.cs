﻿using System.Windows.Controls;

using RegistroElettronicoMVVM.ViewModel;

namespace RegistroElettronicoMVVM.View.Controls
{
	/// <summary>
	/// Interaction logic for ControlMenuList.xaml
	/// </summary>
	public partial class ControlMenuList : UserControl
    {
        public ControlMenuList()
        {
            InitializeComponent();
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ((ViewModelMenuList)DataContext).MenuSelectionChangedExecute?.Invoke(sender);
        }
    }
}
