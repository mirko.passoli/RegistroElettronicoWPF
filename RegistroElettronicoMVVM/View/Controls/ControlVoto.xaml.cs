﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace RegistroElettronicoMVVM.View.Controls
{
	/// <summary>
	/// Logica di interazione per ControlVoto.xaml
	/// </summary>
	public partial class ControlVoto : UserControl
    {
        public SolidColorBrush CircularColor
        {
            get { return (SolidColorBrush)GetValue(CircularColorProperty); }
            set { SetValue(CircularColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CircularColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CircularColorProperty =
            DependencyProperty.Register("CircularColor", typeof(SolidColorBrush), typeof(ControlVoto), new PropertyMetadata(Brushes.Green));
        
        public string Data
        {
            get { return (string)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Data.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(string), typeof(ControlVoto), new PropertyMetadata(string.Empty));
        
        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Description.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(ControlVoto), new PropertyMetadata(string.Empty));
        
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(ControlVoto), new PropertyMetadata(string.Empty));
        
        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
        
        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(ControlVoto), new PropertyMetadata(0.0));
        
        public double Value10
        {
            get { return (double)GetValue(Value10Property); }
            set { SetValue(Value10Property, value); }
        }

        // Using a DependencyProperty as the backing store for Value10.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty Value10Property =
            DependencyProperty.Register("Value10", typeof(double), typeof(ControlVoto), new PropertyMetadata(0.0));
        
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        
        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(ControlVoto), new PropertyMetadata(string.Empty));
        
        public ControlVoto()
        {
            InitializeComponent();
        }
    }
}
