﻿using System.Reflection;
using System.Threading.Tasks;
using System.Windows;

using DesktopToast;

using RegistroElettronicoMVVM.Utils;
using RegistroElettronicoMVVM.View.Windows;

namespace RegistroElettronicoMVVM
{
	/// <summary>
	/// Logica di interazione per App.xaml
	/// </summary>
	public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            SplashScreenHelper.SplashScreen = new SplashWindow();
            SplashScreenHelper.Show();

            MainWindow main;

            main = new MainWindow();
            main.Show();
            SplashScreenHelper.Close();
        }

        public static async Task<bool> ShowToastAsync(string title, string message)
        {
            var request = new ToastRequest
            {
                ToastTitle = title,
                ToastBody = message,
                ShortcutFileName = "Registro Elettronico.lnk",
                ShortcutTargetFilePath = Assembly.GetExecutingAssembly().Location,
                AppId = "RegistroElettronico.Wpf",
            };

            var result = await ToastManager.ShowAsync(request);

            return (result == ToastResult.Activated);
        }
    }
}
