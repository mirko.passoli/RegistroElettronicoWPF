﻿using System.ComponentModel;
using System.Windows.Media;

namespace RegistroElettronicoMVVM.Model
{
	public class Obiettivo : INotifyPropertyChanged
	{
		private string _description;
		private long _idMateria;
		private string _text;
		private double _value;

		public SolidColorBrush Color => GetColor();

		public string Description
		{
			get { return _description; }
			set { _description = value; NotifyPropertyChanged(nameof(Description)); }
		}

		public long IDMateria => _idMateria;

		public double Value
		{
			get { return _value; }
			set { _value = value; NotifyPropertyChanged(nameof(Value)); NotifyPropertyChanged(nameof(Value10)); NotifyPropertyChanged(nameof(Color)); }
		}

		public double Value10 => Value * 10;

		public string Text
		{
			get { return _text; }
			set { _text = value; NotifyPropertyChanged(nameof(Text)); }
		}

		public Obiettivo(long idMateria) => _idMateria = idMateria;

		public event PropertyChangedEventHandler PropertyChanged;
		protected void NotifyPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

		private SolidColorBrush GetColor()
		{
			switch (Classeviva.Voti.GetColor(Value))
			{
				case Classeviva.Color.Blue:
					return Brushes.Blue;
				case Classeviva.Color.Green:
					return Brushes.Green;
				case Classeviva.Color.Red:
					return Brushes.Red;
				default:
					return null;
			}
		}
	}
}
