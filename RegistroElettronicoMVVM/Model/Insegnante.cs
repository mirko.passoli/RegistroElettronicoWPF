﻿using System.ComponentModel;

using Classeviva;

namespace RegistroElettronicoMVVM.Model
{
	public class Insegnante : INotifyPropertyChanged
    {
        #region Fields
        private string _id;
        private string _name;
        #endregion

        #region Properties
        public string ID
        {
            get { return _id; }
            set { _id = value; NotifyPropertyChanged(nameof(ID)); }
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; NotifyPropertyChanged(nameof(Name)); }
        }
        #endregion

        public Insegnante() { }

        public Insegnante(Teacher t)
        {
            ID = t.TeacherId;
            Name = t.TeacherName;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
