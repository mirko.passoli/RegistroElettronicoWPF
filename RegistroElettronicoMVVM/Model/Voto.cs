﻿using System.ComponentModel;
using System.Windows.Media;

using Classeviva;

namespace RegistroElettronicoMVVM.Model
{
	public class Voto : INotifyPropertyChanged
    {
        #region Fields
        private Classeviva.Color _color;
        private string _date;
        private string _display;
        private bool _isSelected;
        private string _notes;
        private string _period;
        private long _periodPos;
        private string _subject;
        private long _subjectID;
        private string _type;
        private long _typePos;
        private double? _value;
        #endregion

        #region Properties
        public Classeviva.Color Color
        {
            get { return _color; }
            set { _color = value; NotifyPropertyChanged(nameof(Color)); }
        }

        public SolidColorBrush ColorBrush => GetVotoColor();

        public string Date
        {
            get { return _date; }
            set { _date = value; NotifyPropertyChanged(nameof(Date)); }
        }

        public string Display
        {
            get { return _display; }
            set { _display = value; NotifyPropertyChanged(nameof(Display)); }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; NotifyPropertyChanged(nameof(IsSelected)); }
        }

        public string Notes
        {
            get { return _notes; }
            set { _notes = value; NotifyPropertyChanged(nameof(Notes)); }
        }

        public string Period
        {
            get { return _period; }
            set { _period = value; NotifyPropertyChanged(nameof(Period)); }
        }

        public long PeriodPos
        {
            get { return _periodPos; }
            set { _periodPos = value; NotifyPropertyChanged(nameof(PeriodPos)); }
        }

        public string Subject
        {
            get { return _subject; }
            set { _subject = value; NotifyPropertyChanged(nameof(Subject)); }
        }

        public long SubjectID
        {
            get { return _subjectID; }
            set { _subjectID = value; NotifyPropertyChanged(nameof(SubjectID)); }
        }

        public string Type
        {
            get { return _type; }
            set { _type = value; NotifyPropertyChanged(nameof(Type)); }
        }

        public long TypePosition
        {
            get { return _typePos; }
            set { _typePos = value; NotifyPropertyChanged(nameof(TypePosition)); }
        }

        public double? Value
        {
            get { return _value; }
            set { _value = value; NotifyPropertyChanged(nameof(Value)); }
        }

        public double? Value10
        {
            get { return Value * 10; }
            set { Value = value / 10; NotifyPropertyChanged(nameof(Value10)); NotifyPropertyChanged(nameof(Value)); }
        }

        public static string[] Voti => new string[] 
        {
            "1", "1+", "1½", "2-",
            "2", "2+", "2½", "3-",
            "3", "3+", "3½", "4-",
            "4", "4+", "4½", "5-",
            "5", "5+", "5½", "6-",
            "6", "6+", "6½", "7-",
            "7", "7+", "7½", "8-",
            "8", "8+", "8½", "9-",
            "9", "9+", "9½", "10-", "10"
        };
        #endregion

        public Voto() { }

        public Voto(Grade g)
        {
            Date = g.EvtDate;
            Color = g.Color;
            Display = g.DisplayValue;
            Notes = g.NotesForFamily;
            Period = g.PeriodDesc;
            PeriodPos = g.PeriodPos;
            Subject = g.SubjectDesc;
            SubjectID = g.SubjectId;
            Type = g.ComponentDesc;
            TypePosition = g.ComponentPos;
            Value = g.DecimalValue;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        
        private SolidColorBrush GetVotoColor()
        {
            if (Value.HasValue)
            {
                switch (Classeviva.Voti.GetColor(Value.Value))
                {
                    case Classeviva.Color.Blue:
                        return Brushes.Blue;
                    case Classeviva.Color.Green:
                        return Brushes.Green;
                    case Classeviva.Color.Red:
                        return Brushes.Red;
                    default:
                        return null;
                }
            }
            else
                return Brushes.Blue;
        }
    }
}
