﻿using System.ComponentModel;

namespace RegistroElettronicoMVVM.Model
{
	public class Login : INotifyPropertyChanged
    {
        private string _expire;
        private string _firstName;
        private string _ident;
        private string _lastName;
        private string _mail;
        private string _password;
        private string _release;
        private string _token;

        public string Expire
        {
            get { return _expire; }
            set { _expire = value; NotifyPropertyChanged(nameof(Expire)); }
        }

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; NotifyPropertyChanged(nameof(FirstName)); }
        }
        
        public string Ident
        {
            get { return _ident; }
            set { _ident = value; NotifyPropertyChanged(nameof(Ident)); }
        }

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; NotifyPropertyChanged(nameof(LastName)); }
        }
        
        public string Mail
        {
            get { return _mail; }
            set { _mail = value; NotifyPropertyChanged(nameof(Mail)); }
        }
        
        public string Password
        {
            get { return _password; }
            set { _password = value; NotifyPropertyChanged(nameof(Password)); }
        }

        public string Release
        {
            get { return _release; }
            set { _release = value; NotifyPropertyChanged(nameof(Release)); }
        }
        
        public string Token
        {
            get { return _token; }
            set { _token = value; NotifyPropertyChanged(nameof(Token)); }
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
