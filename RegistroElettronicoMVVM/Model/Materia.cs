﻿using System.Collections.ObjectModel;
using System.ComponentModel;

using Classeviva;

namespace RegistroElettronicoMVVM.Model
{
	public class Materia : INotifyPropertyChanged
    {
        #region Fields
        private long _id;
        private ObservableCollection<Insegnante> _insegnanti;
        private string _name;
        private string _objectiveText;
        private double? _objectiveValue;
        #endregion

        #region Properties
        public long ID { get => _id; set => _id = value; }

        public ObservableCollection<Insegnante> Insegnanti
        {
            get { return _insegnanti; }
            set { _insegnanti = value; Insegnanti.CollectionChanged += Insegnanti_CollectionChanged; NotifyPropertyChanged(nameof(Insegnanti)); }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                value = value[0] + value.ToLower().Substring(1);
                _name = value;
                NotifyPropertyChanged(nameof(Name));
            }
        }

        public string ObjectiveText
        {
            get { return _objectiveText; }
            set { _objectiveText = value; NotifyPropertyChanged(nameof(ObjectiveText)); }
        }

        public double? ObjectiveValue
        {
            get { return _objectiveValue; }
            set { _objectiveValue = value; NotifyPropertyChanged(nameof(ObjectiveValue)); NotifyPropertyChanged(nameof(ObjectiveValue10)); }
        }

        public double? ObjectiveValue10 => ObjectiveValue * 10;
        #endregion

        public Materia() { }

        public Materia(Subject s)
        {
            ID = s.Id;
            Name = s.Description;

            Insegnanti = new ObservableCollection<Insegnante>();
            foreach (var item in s.Teachers)
                Insegnanti.Add(new Insegnante(item));
        }

        private void Insegnanti_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) => NotifyPropertyChanged(nameof(Insegnanti));

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
