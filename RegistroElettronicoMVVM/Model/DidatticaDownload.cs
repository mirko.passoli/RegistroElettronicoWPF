﻿namespace RegistroElettronicoMVVM.Model
{
	public class DownloadDidattica
    {
        public long ContentId { get; set; }
        public string FilePath { get; set; }
    }
}
