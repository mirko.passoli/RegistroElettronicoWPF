﻿using System;

using Classeviva;

namespace TestAPIClasseviva
{
	class Program
	{
		static void Main(string[] args)
		{
			IResponse login = API.Login("mirko.passoli@gmail.com", "PassoliMirko1");
			if (login as Login != null)
				Console.WriteLine(login + "\n\nVoti:\n");
			else
			{
				Console.WriteLine(login);
				Console.ReadKey();
				return;
			}

			Login logged = (Login)login;
			int sid = logged.GetSID();

			IResponse voti = API.Voti(logged.Token, sid);
			Console.WriteLine(voti);

			Console.WriteLine("\nLezioni:\n");

			IResponse lezioni = API.Lezioni(logged.Token, sid, 20171124, 20171127);
			Console.WriteLine(lezioni);

			Console.WriteLine("\nNote:\n");

			IResponse note = API.Note(logged.Token, sid);
			Console.WriteLine(note);

			Console.WriteLine("\nMaterie:\n");

			IResponse materie = API.Materie(logged.Token, sid);
			Console.WriteLine(materie);

			Console.WriteLine("\nPeriodi:\n");

			IResponse periodi = API.Periodi(logged.Token, sid);
			Console.WriteLine(periodi);

			Console.WriteLine("\nUser info:\n");

			IResponse uinf = API.UInfo(logged.Token, sid);
			Console.WriteLine(uinf);

			Console.WriteLine("\nDidattica:\n");

			IResponse didattica = API.Didattica(logged.Token, sid);
			Console.WriteLine(didattica);

			Console.ReadKey();
		}
	}
}
